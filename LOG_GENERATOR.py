#<<<<<<<<<<<<<<<<<<<<<<<<<<<----------THIS LOG FILE IS USED TO STORE ALL THE DATA WHICH WE WANT TO PRINT------------------>>>>>
#<<<<<<<------WE HAVE TO OPEN ONCE AND CLOSE ONLY ONCE A FILE, THATS WHY IF AND ELSE CONDITION IS USED----->>>>>


import time
count = 0
def log_display(COMMAND_ID,TX_Timestamp,RX_TimeStamp,Status):
    import datetime
    import os
    global obj1
    global count
    
    now = datetime.datetime.now()#storing present date and time into variable
    file = now.date()#storing date in one variable
    filename = str(file)#converting type of date i.e int to str
    if(count == 0):
        #print("the file is first time open")
        print(COMMAND_ID,TX_Timestamp,RX_TimeStamp,Status)#print in console
        current_directory = os.getcwd()#gives current working directory
        final_directory = os.path.join(current_directory, r'PYTHONLOG')#add directory to current_directory
        print(current_directory)
        print(final_directory)
        if not os.path.exists(final_directory):#check whether directory present or not
            os.makedirs(final_directory)#if not present creates a directory
        date_directory = os.path.join(final_directory,filename)#add directory to current_directory
        if not os.path.exists(date_directory):#check whether directory present or not
           os.makedirs(date_directory)#if not present creates a directory
        obj=open('PYTHONLOG\\'+filename+'\\LOG'+".txt", 'a+')#opening a file with present date as a file name
        if os.path.getsize('PYTHONLOG\\'+filename+'\\LOG'+".txt") <= 0:#check whether .txt file is empty or not
           obj.write(">>>>Automation_log<<<<<")
        obj.write("\n")   
        obj.write("<< ")
        now = time.strftime("%c")
        DATA = "%s" % now
        obj.write(DATA)#The method strftime() converts a tuple or struct_time representing a time.
        obj.write(" >>")
        obj.write(',')
        #time_taken=str(time)#CONVERTING INT TO STR
        #obj.write(time_taken)
        obj.write(COMMAND_ID)
        obj.write(',')
        obj.write(str(TX_Timestamp))
        obj.write(',')
        obj.write(str(RX_TimeStamp))
        obj.write(',')
        obj.write(Status)
        obj.flush()#AUTOMATICALLY DATA WILL BE STORED IN FILE, WHEN SOMETHING HAPPEND TO OUR MAIN_FILE OUR DATA WILL NOT BE EFFECTED.
        os.fsync(obj.fileno())# THIS IS ONE FUNCTION WE USED TO PERFORM THE ABOVE OPERATION.
        count+=1
        obj1=obj
    elif('exit'==COMMAND_ID):#closing a file
        obj1.close()
    else:
        #print("file already opened")
        print(COMMAND_ID,TX_Timestamp,RX_TimeStamp,Status)
        obj1.write('\n')
        obj1.write("<< ")
        now = time.strftime("%c")
        DATA = "%s" % now
        obj1.write(DATA)
        obj1.write(" >>")
        obj1.write(',')
        #time_taken=str(time)#CONVERTING INT TO STR
        #obj1.write(time_taken)
        obj1.write(COMMAND_ID)
        obj1.write(',')
        obj1.write(str(TX_Timestamp))
        obj1.write(',')
        obj1.write(str(RX_TimeStamp))
        obj1.write(',')
        obj1.write(Status)
        
        obj1.flush()
        os.fsync(obj1.fileno())
