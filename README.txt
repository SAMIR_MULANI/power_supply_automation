Date: 08-10-2019


---------------------------------------------------------------------------------------------------------------------------------
			AUTOMATION Power_Supply_IT_63223
---------------------------------------------------------------------------------------------------------------------------------

1. Test automation framework : This automation framework Devided in to three parts
	    1>MAIN_UI.py  (Mode Of Opeartion 1 Using GUI)
	    2>CMD_ENGINE.py (Mode Of Operation 2 Using Command prompt )
	    3>Communication_Api.py (Common communication api which will use GUI application as well as Command Prompt)
	    
	Mode Of Operation One:
	---------------------------------------
	# MAIN_UI.py :-this is basically GUI Aapplication that contains two modes of operation.
	
		1>Contineous mode need three parameters ---1)Voltage 2)Current 3)Execution_Time
		2>Power_cycle mode need five parameter ---1)Voltage 2)Current 3)Iteration_No 4)On-Time 5)Off-Time

	   Attached python file is the automation script,execute it with below format.
		Ex: python MAIN_UI.py
		
        	Mode Of Operation Two:
	----------------------------------------
	#CMD_ENGINE.py :-This is basically work with configuration file where we have two modes of operation.
		1>Contineous mode need three parameters ---1)Voltage 2)Current 3)Execution_Time
		2>Power_cycle mode need five parameter ---1)Voltage 2)Current 3)Iteration_No 4)On-Time 5)Off-Time

	   Attached python file is the automation script,execute it with below format.
		Ex: python CMD_ENGINE.py 
                   



2. Test results and logs : Each execution of test automation framework generates below logs. These logs are located in folder<logs>.
		
PREREQUISITES:-
   --------------
	1. Python version 3.6.6 or latest python version from "https://www.python.org/"
	2 .Install following packagesto run this automation file.
		--->python -m pip pip install pyqt5
		--->python -m pip install pyserial
		--->>python pip install mysqlclient


Note:
-------

*Limitations: 1>CMD_ENGINE.py is to be tested with power supply.
	    2> According to the new updated(Related to code optimization) code has to be change.