# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!
from PyQt5.QtWidgets import QApplication, QSplashScreen
from PyQt5 import QtCore, QtGui, QtWidgets
from Communication_Api import *

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QSplashScreen
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import threading

import sys
import time

class Ui_MainWindow(object):
    def setupUi(self, Dialog):
        self.PillKillThreadOne = threading.Event()
        self.PillKillThreadTwo = threading.Event()
        self.PillKillThreadThree = threading.Event()
        self.Target = Comunication_Api()
        Dialog.setObjectName("Dialog")
        Dialog.resize(428, 178)
        self.pushButton_Thread1 = QtWidgets.QPushButton(Dialog)
        self.pushButton_Thread1.setGeometry(QtCore.QRect(20, 30, 75, 23))
        self.pushButton_Thread1.setObjectName("pushButton_Thread1")
        self.pushButton_Thread2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_Thread2.setGeometry(QtCore.QRect(170, 30, 75, 23))
        self.pushButton_Thread2.setObjectName("pushButton_Thread2")
        self.pushButton_Thread3 = QtWidgets.QPushButton(Dialog)
        self.pushButton_Thread3.setGeometry(QtCore.QRect(320, 30, 75, 23))
        self.pushButton_Thread3.setObjectName("pushButton_Thread3")
        self.pushButton_Thread1_2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_Thread1_2.setGeometry(QtCore.QRect(20, 100, 75, 23))
        self.pushButton_Thread1_2.setObjectName("pushButton_Thread1_2")
        self.pushButton_Thread1_3 = QtWidgets.QPushButton(Dialog)
        self.pushButton_Thread1_3.setGeometry(QtCore.QRect(170, 100, 75, 23))
        self.pushButton_Thread1_3.setObjectName("pushButton_Thread1_3")
        self.pushButton_Thread1_4 = QtWidgets.QPushButton(Dialog)
        self.pushButton_Thread1_4.setGeometry(QtCore.QRect(320, 100, 75, 23))
        self.pushButton_Thread1_4.setObjectName("pushButton_Thread1_4")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

        self.pushButton_Thread1.clicked.connect(lambda:self.thread_one())
        self.pushButton_Thread2.clicked.connect(lambda:self.thread_two())
        self.pushButton_Thread3.clicked.connect(lambda:self.thread_three())

        self.pushButton_Thread1_2.clicked.connect(lambda:self.thread_one_stop())
        self.pushButton_Thread1_3.clicked.connect(lambda:self.thread_Two_stop())
        self.pushButton_Thread1_4.clicked.connect(lambda:self.thread_three_stop())





    def thread_one(self):
        Data1 = "12"
        Data2 = "22"

        
        self.Thread_One = threading.Thread(target = self.Target.Parameter_To_Update,args = (self.PillKillThreadOne,Data1,Data2))
        
        if self.PillKillThreadOne.is_set():
            print("---------------Events ---Are--thread_one--Setted---------")
            self.PillKillThreadOne.clear()
            self.Thread_One.start()
        else:
            print("---------------Events ---Are--thread_one--Not_Setted-----")
            self.Thread_One.start()
            self.PillKillThreadOne.clear()
            
        


    def thread_two(self):
         Data3 = "33"
         Data4 = "44"
         
         self.Thread_two = threading.Thread(target = self.Target.Parameter_To_Update,args = (self.PillKillThreadTwo,Data3,Data4))
         
         if self.PillKillThreadTwo.is_set():
            print("---------------Events__thread_two--Are----Setted---------")
            self.PillKillThreadTwo.clear()
            self.Thread_two.start()
         else:
            print("---------------Events -thread_two--Are----Not_Setted-----")
            self.PillKillThreadTwo.clear()
            self.Thread_two.start()

         
    def thread_three(self):
         Data5 = "55"
         Data6 = "66"
        
         self.Thread_three = threading.Thread(target = self.Target.Parameter_To_Update,args = (self.PillKillThreadThree,Data5,Data6))
         
         if self.PillKillThreadThree.is_set():
            print("---------------Events ---Are--thread_three--Setted---------")
            self.PillKillThreadThree.clear()
            self.Thread_three.start()
         else:
            print("---------------Events ---Are--thread_three--Not_Setted-----")
            self.PillKillThreadThree.clear()
            self.Thread_three.start()



         

    def thread_one_stop(self):
        self.PillKillThreadOne.set()
        self.Thread_One.join()

    def thread_Two_stop(self):
        self.PillKillThreadTwo.set()
        self.Thread_two.join()
        
    def thread_three_stop(self):
        self.PillKillThreadThree.set()
        self.Thread_three.join()


    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.pushButton_Thread1.setText(_translate("Dialog", "Thread1"))
        self.pushButton_Thread2.setText(_translate("Dialog", "Thread2"))
        self.pushButton_Thread3.setText(_translate("Dialog", "Thread3"))
        self.pushButton_Thread1_2.setText(_translate("Dialog", "Kill_Thread1"))
        self.pushButton_Thread1_3.setText(_translate("Dialog", "Kill_Thread2"))
        self.pushButton_Thread1_4.setText(_translate("Dialog", "Kill_Thread3"))
        

if __name__ == '__main__':
                        app = QtWidgets.QApplication(sys.argv)
                        MainWindow = QtWidgets.QMainWindow()
                        splash = QSplashScreen(QPixmap('Tessolve_normal.jpg'))
                        splash.show()
                        window1 = QtWidgets.QWidget()
                        time.sleep(0.2)
                        splash.finish(window1)
                        window = Ui_MainWindow()
                        window.setupUi(MainWindow)
                        MainWindow.show()
                        sys.exit(app.exec_())

