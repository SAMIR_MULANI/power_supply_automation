#<<<<<<<<<<<<<<<<<<<<<<<<<<<----------THIS LOG FILE IS USED TO STORE ALL THE DATA WITH RESULT------------------>>>>>
#<<<<<<<------WE HAVE TO OPEN ONCE AND CLOSE ONLY ONCE A FILE, THATS WHY IF AND ELSE CONDITION IS USED----->>>>>

count=0
def test_result(Tx_Voltage,Rx_Voltage):
    import datetime
    import os
    global obj1
    global count
    now = datetime.datetime.now()#storing present date and time into variable
    file=now.date()#storing date in one variable
    filename=str(file)#converting type of date i.e int to str 
    if(count==0):
        #print("the file is first time open")
        print(Tx_Voltage,'\t\t', Rx_Voltage)#print in console
        current_directory = os.getcwd()#gives current working directory
        final_directory = os.path.join(current_directory, r'logs')#add directory to current_directory
        if not os.path.exists(final_directory):#check whether directory present or not
            os.makedirs(final_directory)#if not present creates a directory
        date_directory = os.path.join(final_directory,filename)#add directory to current_directory
        if not os.path.exists(date_directory):#check whether directory present or not
           os.makedirs(date_directory)#if not present creates a directory
        obj=open('logs\\'+filename+'\\test_result'+".txt", 'a+')#opening a file with present date as a file name
        if os.path.getsize('logs\\'+filename+'\\test_result'+".txt") <= 0:#check whether .txt file is empty or not
            #if empty add headers
            obj.write('\n')
            obj.write('----------------------------------------------------------------')
            obj.write('\n')
            obj.write("\ttime\t\t")
            obj.write("Tx_Voltage\t\t")
            obj.write("Rx_Voltage")
            obj.write('\n')
            obj.write('----------------------------------------------------------------')
            obj.write('\n')
        obj.write('\n')
        obj.write("<< ")
        obj.write(now.strftime("%Y-%m-%d %H:%M:%S"))#The method strftime() converts a tuple or struct_time representing a time.
        obj.write(" >>")
        obj.write('   ')
        obj.write(Tx_Voltage)#writing test in file
        #obj.write('\t\t')
        obj.write('\t\t'+Rx_Voltage)#writing result whether test pass/fail in file
        obj.flush()#AUTOMATICALLY DATA WILL BE STORED IN FILE, WHEN SOMETHING HAPPEND TO OUR MAIN_FILE OUR DATA WILL NOT BE EFFECTED.
        os.fsync(obj.fileno())# THIS IS ONE FUNCTION WE USED TO PERFORM THE ABOVE OPERATION.
        count+=1
        obj1=obj
    elif('exit'==Tx_Voltage):#closing a file
        obj1.close()
    else:
        #print("file already opened")
        print(Tx_Voltage,'\t\t', Rx_Voltage)
        obj1.write('\n')
        obj1.write("<< ")
        obj1.write(now.strftime("%Y-%m-%d %H:%M:%S"))
        obj1.write(" >>")
        obj1.write('   ')
        obj1.write(Tx_Voltage)#writing test in file
        obj1.write('\t\t')
        obj1.write(Rx_Voltage)#writing result whether test pass/fail in file
        obj1.flush()
        os.fsync(obj1.fileno())
    
