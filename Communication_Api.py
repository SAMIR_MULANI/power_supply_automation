import sys
from Main_Gui import*
from datetime import date
import datetime
from test_result import*


#This order can set SOURCE METER as remote control mode
SYS_REMOTE = "SYST:REM\n"
#This order can read information about power supply
IDN_SYS_INFO ="*IDN?\n"
#This order can be used to select the channel
FIRST_CHANNEL  = "INST FIR\n"
SECOND_CHANNEL = "INST SECO\n"
THIRD_CHANNEL  = "INST THI\n"
CHANNEL_INFO = "INST?\n"
SET_VOLTAGE = "VOLT" #Along with voltage value as a argument
VOLTAGE_STATUS = "VOLT?\n"
SET_CURRENT = "CURR" #Along with current value as a argument
CURRENT_STATUS = "CURR?\n"
'''
This order can check the output state for current channel: ON/OFF
OUTP
----

'''
CURRENT_CHANNEL_ON = "OUTP 1\n"
CURRENT_CHANNEL_OFF = "OUTP 0\n"
CURRENT_CHANNEL_STATUS = "OUTP?\n"
global Update_Time_Con_CH_1
global Update_Time_Con_CH_2
global Update_Time_Con_CH_3

global Voltage_Channel




    

class Communication_Api():

    
    def __init__(self):
        '''
        
        Function -- Constructer
        ------
        '''
        self.lock = threading.Lock()

        ################################
        self.START_BUTTON_CH_1  = False
        self.PAUSE_BUTTON_CH_1  = False
        self.RESUME_BUTTON_CH_1 = False
        ################################
        self.START_BUTTON_CH_2  = False
        self.PAUSE_BUTTON_CH_2  = False
        self.RESUME_BUTTON_CH_2 = False
        ################################
        self.START_BUTTON_CH_3  = False
        self.PAUSE_BUTTON_CH_3  = False
        self.RESUME_BUTTON_CH_3 = False
        ################################
        
        self.Left_Time       = 0
        self.Left_Time_ch2   = 0
        self.Left_Time_Ch3   = 0
        self.Pause_Time      = 0
        self.Pause_Time_ch2  = 0
        self.Pause_Time_Three= 0
        self.Start_Time      = 0
        self.Start_Time_ch2  = 0
        self.Start_Time_Three= 0
        self.Exicution_Time_Channel_2 = 0
        
        ################################
        
         
    
    def COM_PORT(self,Baudrate,Current_Com_Port):
        
        if not Baudrate == 0:
            Baudrate = 4800
        else:
            print(Baudrate)


            
        print(Current_Com_Port)
        try:
            self.ser=serial.Serial(port=Current_Com_Port,baudrate=Baudrate,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_TWO, bytesize=serial.EIGHTBITS, writeTimeout = 0)
            if self.ser.isOpen():
                
                        print("Com_Port_IS allready opened")
                        Port_OPEN = "Connected.."
                        self.Com_Port_Opend = "Com_Port_Opend_Already_Opened"
                        Ser_Data = SYS_REMOTE.encode()
                        self.ser.write(Ser_Data)
                        print("......................SUCESSFULLY SEND------> "+SYS_REMOTE)
                        return Port_OPEN        
                
        except serial.SerialException:
                self.ser.close()
                Port_Closed = "Disconnected..."

                
                return Port_Closed
            
        
    def SEND_MSG_CHANNEL_ONE_PARAMETERS(self,Voltage_Channel_1,Exicution_Time_Channel_1):
        """
            This function create the message format
            Parameters
            ----------
            arg : int

            Returns
            ----------
            bytes
                Message that is sended through com port.

            CURRENT-AND-VOLTAGE-VALIDATION FOR CHANNEL-1
            Channel 1 maximum voltage = 100
            minimum = 0
            
            -------------------------------------------
        """

        if int(Voltage_Channel_1)<0 or int(Voltage_Channel_1) >33 or str(Voltage_Channel_1).isalpha():
            print("Enter Valied voltages in between 0-100 voltage")
            VALIDATION_FOR_CHANNEL = "####### :-"+"VALIDATION FOR CHANNEL-1 Enter Voltage in 0-32 vtg"
            print(VALIDATION_FOR_CHANNEL)
            return VALIDATION_FOR_CHANNEL


        ##            
##            if int(Current_Channel_1)<0 or int(Current_Channel_1) >3 or str(Current_Channel_1).isalpha():
##                print("Enter Valied Current in between 0-3 Amp")
##                VALIDATION_FOR_CHANNEL = "####### :-"+"VALIDATION FOR CHANNEL-1 Current in between 0-3 Amp"
##                print(VALIDATION_FOR_CHANNEL)
##                return VALIDATION_FOR_CHANNEL
                

                               
        '''
        CHANNEL VOLTAGE
        '''
        self.Voltage_Channel    = Voltage_Channel_1
        self.SET_VOLTAGE_CH_ONE = SET_VOLTAGE+" "+str(Voltage_Channel_1)+"\n"
        self.SET_VOLTAGE_ENCO       = self.SET_VOLTAGE_CH_ONE.encode()
        print("GROUP DATA VOLTAGE DATA>>>",self.SET_VOLTAGE_CH_ONE)
        print("ENCODED VOLTAGE DATA",self.SET_VOLTAGE_ENCO)

        self.Exicution_Time_Channel_1 = Exicution_Time_Channel_1

        
            
    def UPDATE_FLAG_PAUSE(self):
        self.PAUSE_BUTTON_CH_1 = True

        
    def UPDATE_FLAG_RESUME(self):
        self.PAUSE_BUTTON_CH_1 = False
        
    def UPDATE_FLAG_SEND(self):
         self.PAUSE_BUTTON_CH_1 = False
        

    def Update_Pause_Time(self,PAUSE_TIME):
        self.Pause_Time = PAUSE_TIME
        print("Update_Resume_Time",self.Pause_Time)
        
    def Updated_Start_Time(self,START_TIME):
        self.Start_Time = START_TIME
        print("Updated_Start_Time", self.Start_Time)
        
    
    def Update_Time(self):
        print("Inside the Update Time")
        
        self.Left_Time = self.Pause_Time- self.Start_Time
        print("Update_Time---Update_Time---Update_Time--Update_Time",self.Left_Time)
        
                    
    def SEND_MSG_CHANNEL_ONE_CON(self):
##            print("Left_TimeLeft_TimeLeft_Time",self.Left_Time)
##            
##            timeout = time.time() + 60*self.Exicution_Time_Channel_1
##            if self.Left_Time:
##                timeout = timeout-self.Left_Time
##                print("timeout----timeout---timeout",timeout) 

            
##            while True:
##                if self.PAUSE_BUTTON_CH_1:
##                    break
##
##                if time.time() > timeout:
##                    Time_Out = "Time_Over_CH-1"
##                                    #return Time_Out
##                    break
##                
##                time.sleep(1)
##                print("!!!!!!!!!!!!!!!!!!SEND_MSG_CHANNEL_ONE_CON!!!!!!!!!!!!!!!!!!!!")
            #print("self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1",self.PAUSE_BUTTON_CH_1)

            #else:
            #print("<<<<<<<<<<<<<<IDN_SYS_INFO INFO>>>>>>>>>>>>>>>>>>>>",IDN_SYS_INFO)
            #print("self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1",self.PAUSE_BUTTON_CH_1)
            #self.lock.acquire()
##            Ser_Data = IDN_SYS_INFO.encode()
##            self.ser.write(Ser_Data)
##            test_result(str(IDN_SYS_INFO),"0")
##            print("......................SUCESSFULLY SEND------> "+IDN_SYS_INFO)
##            #self.Updating_Logs(IDN_SYS_INFO)
##            Received_Msg= self.ser.readline()
##            if Received_Msg:
##                        print(Received_Msg)
##                        print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
##                        #self.Updating_Logs(Received_Msg)
##                        test_result(str(Received_Msg),"0")
####                        Received_Msg = str(Received_Msg)
####                        return Received_Msg
##
##            else:
##                        print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
##                        Rec_Info = "No response from power supply"
##                        #self.Updating_Logs(Rec_Info)
##                        test_result(Rec_Info,"0")
##                        #return Rec_Info
##                        
##            '''
##            Selecting the channel
##            '''
##            #self.lock.release()
##            
##            Ser_Data = FIRST_CHANNEL.encode()
##            self.ser.write(Ser_Data)
##            #self.Updating_Logs(FIRST_CHANNEL)
##            test_result(str(FIRST_CHANNEL),"0")
##            '''
##            Getting the channel INFO
##            '''
##            self.lock.acquire()
##            
##            print("<<<<<<<<<<<<<<Getting the channel INFO>>>>>>>>>>>>>>>>>>>>",CHANNEL_INFO)
##            Ser_Data = CHANNEL_INFO.encode()
##            self.ser.write(Ser_Data)
##            test_result(str(CHANNEL_INFO),"0")
##            
##            Received_Msg= self.ser.read()
##            if Received_Msg:
##                            print(Received_Msg)
##                            print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
##                            #self.Updating_Logs(Received_Msg)
##                            test_result(str(Received_Msg),"0")
##                            
##                            #Received_Msg = str(Received_Msg)
##                            #return Received_Msg
##
##            else:
##                         print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
##                         Rec_Info = "No response from power supply"
##                         #self.Updating_Logs(Rec_Info)
##                         test_result(Rec_Info,"0")
##                         #return Rec_Info
##            #self.lock.release()
##            
##
##            '''
##            MSG_FORMAT
##            ----------
##            '''
##                #SETTING TIME AS A MIN TO RUN THE WHILE LOOP
##            
##            
##            print("Left_TimeLeft_TimeLeft_Time",self.Left_Time)
##            
##            timeout = time.time() + 60*self.Exicution_Time_Channel_1
##            if self.Left_Time:
##                timeout = timeout-self.Left_Time
##                print("timeout----timeout---timeout",timeout)
##            print("self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1",self.PAUSE_BUTTON_CH_1)
##                
##            while True:
##                
##                                '''
##                                seting the channel voltage
##                                '''
##                                self.lock.acquire()
##                                #print("<<<<<<<<<<<<<<> seting the channel voltage>>>>>>>>>>>>>>>>>>>",self.SET_VOLTAGE_ENCO)
##                                start_time = time.time()
##                                CURRENT_TIME = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S.%f')
##                                print("<<<<<<<<<<<<<<<<<<<CURRENT_TIME<<<<<<<<<<<<<<<<",CURRENT_TIME)
##                                self.ser.write(self.SET_VOLTAGE_ENCO)
##                                print("SUCESSFULLY SEND---------------->",self.SET_VOLTAGE_ENCO)
##                                #self.Updating_Logs(self.SET_VOLTAGE_ENCO)
##                                
##                                
##                                time.sleep(2)
##                                if self.PAUSE_BUTTON_CH_1:
##                                    print("<<<<<<RESUMING------------THREAD>>>>>>>")
##                                    
##                                    break
##
##                                
##                                if time.time() > timeout:
##                                    Time_Out = "Time_Over_CH-1"
##                                    #return Time_Out
##                                    break
##
##
##                                '''
##                                Checking the current channel voltage
##                                '''
##                                
##                                Ser_Data = VOLTAGE_STATUS.encode()
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND---------------->",Ser_Data)
##                                Received_Msg= self.ser.readline()
##                                if Received_Msg:
##                                    print(Received_Msg)
##                                    print("SUCESSFULLY RECEIVED VOLTAGE_STATUS------->",Received_Msg)
##                                    #self.Updating_Logs(Received_Msg)
##                                    
##                                    #Received_Msg = str(Received_Msg)
##                                    #return Received_Msg
##                                else:
##                                    print(">>>There is No response from power supply<<<<")
##                                    Rec_Info = "No response from power supply"
##                                    #self.Updating_Logs(Received_Msg)
##                                    test_result(Rec_Info,"0")
##
##                                test_result(str(self.SET_VOLTAGE_ENCO),str(Received_Msg))
##
##                                self.lock.release()
                                    #return Rec_Info
##            self.lock.acquire()
##            Ser_Data = IDN_SYS_INFO.encode()
##            self.ser.write(Ser_Data)
##            
##            test_result(str(IDN_SYS_INFO),"0")
##            print("......................SUCESSFULLY SEND------> "+IDN_SYS_INFO)
##            self.Updating_Logs(IDN_SYS_INFO)
##            Received_Msg= self.ser.readline()
##            if Received_Msg:
##                        print(Received_Msg)
##                        print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
##                        self.Updating_Logs(Received_Msg)
##                        test_result(str(Received_Msg),"0")
####                        Received_Msg = str(Received_Msg)
####                        return Received_Msg
##
##            else:
##                        print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
##                        Rec_Info = "No response from power supply"
##                        self.Updating_Logs(Rec_Info)
##                        test_result(Rec_Info,"0")
##                        #return Rec_Info
##            self.lock.release()
##            
##            '''
##            Selecting the channel
##            '''
##            Ser_Data = FIRST_CHANNEL.encode()
##            self.ser.write(Ser_Data)
##            self.Updating_Logs(FIRST_CHANNEL)
##            test_result(str(FIRST_CHANNEL),"0")
##            '''
##            Getting the channel INFO
##            '''
##            print("<<<<<<<<<<<<<<Getting the channel INFO>>>>>>>>>>>>>>>>>>>>",CHANNEL_INFO)
##
##            #self.lock.acquire()
##            Ser_Data = CHANNEL_INFO.encode()
##            self.lock.acquire()
##            self.ser.write(Ser_Data)
##            test_result(str(CHANNEL_INFO),"0")
##            
##            Received_Msg= self.ser.read()
##            if Received_Msg:
##                            print(Received_Msg)
##                            print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
##                            self.Updating_Logs(Received_Msg)
##                            test_result(str(Received_Msg),"0")
##                            
##                            #Received_Msg = str(Received_Msg)
##                            #return Received_Msg
##
##            else:
##                         print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
##                         Rec_Info = "No response from power supply"
##                         self.Updating_Logs(Rec_Info)
##                         test_result(Rec_Info,"0")
##                         #return Rec_Info
##
##            #self.lock.release()

            '''
            MSG_FORMAT
            ----------
            '''
                #SETTING TIME AS A MIN TO RUN THE WHILE LOOP
            
            print("Left_TimeLeft_TimeLeft_Time",self.Left_Time)
            
            timeout = time.time() + 60*self.Exicution_Time_Channel_1
            if self.Left_Time:
                timeout = timeout-self.Left_Time

            print("timeout----timeout---timeout",timeout)
                
            while True:
                                
                                print("self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1",self.PAUSE_BUTTON_CH_1)
                                '''
                                seting the channel voltage
                                '''
                                #print("<<<<<<<<<<<<<<> seting the channel voltage>>>>>>>>>>>>>>>>>>>",self.SET_VOLTAGE_ENCO)
                                start_time = time.time()
                                CURRENT_TIME = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S.%f')
                                print("<<<<<<<<<<<<<<<<<<<CURRENT_TIME<<<<<<<<<<<<<<<<",CURRENT_TIME)
                                self.lock.acquire()
                                try:
                                    self.ser.write(self.SET_VOLTAGE_ENCO)
                                finally:
                                    self.lock.release()
                                print("SUCESSFULLY SEND---------------->",self.SET_VOLTAGE_ENCO)
                                self.Updating_Logs(self.SET_VOLTAGE_ENCO)
                                
                                
                                time.sleep(2)
                                if self.PAUSE_BUTTON_CH_1:
                                    print("<<<<<<RESUMING------------THREAD>>>>>>>")
                                    
                                    break

                                
                                if time.time() > timeout:
                                    Time_Out = "Time_Over_CH-1"
                                    #return Time_Out
                                    break


                                '''
                                Checking the current channel voltage
                                '''
                                
                                Ser_Data = VOLTAGE_STATUS.encode()
                                self.lock.acquire()
                                try:
                                    self.ser.write(Ser_Data)
                                    
                                    print("SUCESSFULLY SEND---------------->",Ser_Data)
                                    Received_Msg= self.ser.readline()
                                    if Received_Msg:
                                        print(Received_Msg)
                                        print("SUCESSFULLY RECEIVED VOLTAGE_STATUS------->",Received_Msg)
                                        self.Updating_Logs(Received_Msg)
                                        
                                        #Received_Msg = str(Received_Msg)
                                        #return Received_Msg
                                    else:
                                        print(">>>There is No response from power supply<<<<")
                                        Rec_Info = "No response from power supply"
                                        self.Updating_Logs(Received_Msg)
                                        test_result(Rec_Info,"0")
                                finally:
                                    self.lock.release()
                                test_result(str(self.SET_VOLTAGE_ENCO),str(Received_Msg))
                                
##                                time.sleep(2)
##                                
##                                Ser_Data = CURRENT_CHANNEL_ON.encode()
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND--------->",CURRENT_CHANNEL_ON)
##                                Ser_Data = CURRENT_CHANNEL_STATUS.encode()
##                                self.ser.write(Ser_Data)
##
##                                Received_Msg= self.ser.read()
##                                if Received_Msg:
##                                    print(Received_Msg)
##                                    print("SUCESSFULLY RECEIVED------->",Received_Msg)
##                                    #Received_Msg = str(Received_Msg)
##                                    #return Received_Msg
##                                else:
##                                    print(">>>There is No response from power supply<<<")
##                                    Rec_Info = "No response from power supply"
##                                    #return Rec_Info
##                                time.sleep(2)
##
##                                Ser_Data = CURRENT_CHANNEL_OFF.encode()
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND--------->",CURRENT_CHANNEL_OFF)
##                                Ser_Data = CURRENT_CHANNEL_STATUS.encode()
##                                time.sleep(2)
##
##                                
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND--CURRENT_CHANNEL_STATUS------->",CURRENT_CHANNEL_STATUS)
##
##                                Received_Msg= self.ser.read()
##                                if Received_Msg:
##                                    print(Received_Msg)
##                                    print("SUCESSFULLY RECEIVED------->",Received_Msg)
##                                    #Received_Msg = str(Received_Msg)
##                                    #return Received_Msg
##                                else:
##                                    print(">>>There is No response from power supply<<<")
##                                    Rec_Info = "No response from power supply"
##                                    #return Rec_Info
##                                time.sleep(2)

    def  SEND_MSG_CHANNEL_POW_PARAMETER(self,Iteration_No_Channel_1,On_Time_Channel_1,Off_Time_Channel_1,Voltage_Channel_1):
                if int(Voltage_Channel_1)<0 or int(Voltage_Channel_1) >33 or str(Voltage_Channel_1).isalpha():
                            VALIDATION_FOR_CHANNEL = "####### :-"+"VALIDATION FOR CHANNEL-1 Enter Voltage in 0-32 vtg"
                            print(VALIDATION_FOR_CHANNEL)
                            return VALIDATION_FOR_CHANNEL


                                       
                '''
                CHANNEL VOLTAGE
                '''
                self.Voltage_Channel    = Voltage_Channel_1
                self.SET_VOLTAGE_CH_ONE = SET_VOLTAGE+" "+str(Voltage_Channel_1)+"\n"
                self.SET_VOLTAGE_ENCO       = self.SET_VOLTAGE_CH_ONE.encode()
                print("GROUP DATA VOLTAGE DATA>>>",self.SET_VOLTAGE_CH_ONE)
                print("ENCODED VOLTAGE DATA",self.SET_VOLTAGE_ENCO)
                self.Iteration_No_Channel_1 = Iteration_No_Channel_1
                self.On_Time_Channel_1 = On_Time_Channel_1
                self.Off_Time_Channel_1 = Off_Time_Channel_1
                
            
    def SEND_MSG_CHANNEL_POW_CYCLE(self):

                '''
                Non_Contineous-Mode-OPERATION
                -----------------------------
                '''
               
                self.lock.acquire()
                Ser_Data = IDN_SYS_INFO.encode()
                self.ser.write(Ser_Data)
                print("......................SUCESSFULLY SEND------> "+IDN_SYS_INFO)
                Received_Msg= self.ser.readline()
                if Received_Msg:
                            print(Received_Msg)
                            print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
    ##                        Received_Msg = str(Received_Msg)
    ##                        return Received_Msg

                else:
                            print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                            Rec_Info = "No response from power supply"
                            #return Rec_Info

                '''
                Selecting the channel
                '''
                Ser_Data = SECOND_CHANNEL.encode()
                self.ser.write(Ser_Data)
                '''
                Getting the channel INFO
                '''
                print("<<<<<<<<<<<<<<Getting the channel INFO>>>>>>>>>>>>>>>>>>>>",CHANNEL_INFO)
                Ser_Data = CHANNEL_INFO.encode()
                self.ser.write(Ser_Data)
                Received_Msg= self.ser.read()
                
                if Received_Msg:
                                print(Received_Msg)
                                print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
                                #Received_Msg = str(Received_Msg)
                                #return Received_Msg

                else:
                             print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                             Rec_Info = "No response from power supply"
                             #return Rec_Info

                self.lock.release()

                             
                Count = 0

                print(">>>>>>>>>>>>>NON_CONTINEOUS MODE<<<<<<<<<<<",self.SET_VOLTAGE_ENCO)
                while Count< int(self.Iteration_No_Channel_1):
                            self.stop_threads = False
                            #Power_Cycle = threading.Thread(target = self.Non_Contineous_Mode_Channel_1) 
                            Power_Cycle.start() 
                            time.sleep(int(self.On_Time_Channel_1)) 
                            self.stop_threads = True
                            Power_Cycle.join() 
                            print('thread killed')
                            Count = Count+1
                            print("Iteration_No_Channel_1",Count)
                            time.sleep(int(self.Off_Time_Channel_1))


    def Non_Contineous_Mode_Channel_1(self):
              
                while True:
                    if self.stop_threads: 
                        break

                    self.lock.acquire()

                    start_time = time.time()
                    CURRENT_TIME = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S.%f')
                    print("<<<<<<<<<<<<<<<<<<<CURRENT_TIME<<<<<<<<<<<<<<<<",CURRENT_TIME)
                    self.ser.write(self.SET_VOLTAGE_ENCO)
                    print("SUCESSFULLY SEND---------------->",self.SET_VOLTAGE_ENCO)
                    print("INSIDE THE Non_Contineous_Mode_Channel_1")
                    

                    '''
                    Checking the current channel voltage
                    '''
                                        
                    self.Ser_Data = VOLTAGE_STATUS.encode()
                    self.ser.write(self.Ser_Data)
                    print("SUCESSFULLY SEND---------------->",self.Ser_Data)
                    Received_Msg= self.ser.readline()
                    if Received_Msg:
                                print(Received_Msg)
                                print("SUCESSFULLY RECEIVED VOLTAGE_STATUS------->",Received_Msg)
                                            #Received_Msg = str(Received_Msg)
                                            #resturn Received_Msg
                    else:
                                print(">>>There is No response from power supply<<<<")
                                Rec_Info = "No response from power supply"


                    self.lock.release()




        #####################################################################channel 2 start ###########################################################

    def SEND_MSG_CHANNEL_TWO_PARAMETERS(self,Voltage_Channel_2,Exicution_Time_Channel_2):
        """
            This function create the message format
            Parameters
            ----------
            arg : int

            Returns
            ----------
            bytes
                Message that is sended through com port.

            CURRENT-AND-VOLTAGE-VALIDATION FOR CHANNEL-1
            Channel 1 maximum voltage = 100
            minimum = 0
            
            -------------------------------------------
        """

        if int(Voltage_Channel_2)<0 or int(Voltage_Channel_2) >33 or str(Voltage_Channel_2).isalpha():
            VALIDATION_FOR_CHANNEL = "####### :-"+"VALIDATION FOR CHANNEL-1 Enter Voltage in 0-32 vtg"
            print(VALIDATION_FOR_CHANNEL)
            return VALIDATION_FOR_CHANNEL


        ##            
##            if int(Current_Channel_1)<0 or int(Current_Channel_1) >3 or str(Current_Channel_1).isalpha():
##                print("Enter Valied Current in between 0-3 Amp")
##                VALIDATION_FOR_CHANNEL = "####### :-"+"VALIDATION FOR CHANNEL-1 Current in between 0-3 Amp"
##                print(VALIDATION_FOR_CHANNEL)
##                return VALIDATION_FOR_CHANNEL
                

                               
        '''
        CHANNEL VOLTAGE
        '''
        self.Voltage_Channel    = Voltage_Channel_2
        self.SET_VOLTAGE_CH_TWO = SET_VOLTAGE+" "+str(Voltage_Channel_2)+"\n"
        self.SET_VOLTAGE_ENCO_CH2       = self.SET_VOLTAGE_CH_TWO.encode()
        print("GROUP DATA VOLTAGE DATA>>>",self.SET_VOLTAGE_CH_TWO)
        print("ENCODED VOLTAGE DATA",self.SET_VOLTAGE_ENCO_CH2)

        self.Exicution_Time_Channel_2 = Exicution_Time_Channel_2

        
            
    def UPDATE_FLAG_PAUSE_CH2(self):
        self.PAUSE_BUTTON_CH_2 = True

        
    def UPDATE_FLAG_RESUME_CH2(self):
        self.PAUSE_BUTTON_CH_2 = False
        
    def UPDATE_FLAG_SEND_CH2(self):
         self.PAUSE_BUTTON_CH_2 = False
        

    def Update_Pause_Time_CH2(self,PAUSE_TIME):
        self.Pause_Time_ch2 = PAUSE_TIME
        print("Update_Resume_Time",self.Pause_Time)
        
    def Updated_Start_Time_CH2(self,START_TIME):
        self.Start_Time_ch2 = START_TIME
        print("Updated_Start_Time", self.Start_Time)
        
    
    def Update_Time_CH2(self):
        print("Inside the Update Time")
        
        self.Left_Time_ch2 = self.Pause_Time_ch2- self.Start_Time_ch2
        print("Update_Time---Update_Time---Update_Time--Update_Time",self.Left_Time_ch2)
        
                    
    def SEND_MSG_CHANNEL_TWO_CON(self):
##            print("Left_TimeLeft_TimeLeft_Time",self.Left_Time)
##            
##            timeout = time.time() + 60*self.Exicution_Time_Channel_1
##            if self.Left_Time:
##                timeout = timeout-self.Left_Time
##                print("timeout----timeout---timeout",timeout) 

            
##            while True:
##                if self.PAUSE_BUTTON_CH_1:
##                    break
##
##                if time.time() > timeout:
##                    Time_Out = "Time_Over_CH-1"
##                                    #return Time_Out
##                    break
##                
##                time.sleep(1)
##                print("!!!!!!!!!!!!!!!!!!SEND_MSG_CHANNEL_ONE_CON!!!!!!!!!!!!!!!!!!!!")
            #print("self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1",self.PAUSE_BUTTON_CH_1)

            #else:
            #print("<<<<<<<<<<<<<<IDN_SYS_INFO INFO>>>>>>>>>>>>>>>>>>>>",IDN_SYS_INFO)
            self.lock.acquire()
            try:
                Ser_Data = IDN_SYS_INFO.encode()
                self.ser.write(Ser_Data)
                test_result(str(IDN_SYS_INFO),"0")
                print("......................SUCESSFULLY SEND------> "+IDN_SYS_INFO)
                self.Updating_Logs(IDN_SYS_INFO)
                Received_Msg= self.ser.readline()
                if Received_Msg:
                            print(Received_Msg)
                            print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
                            self.Updating_Logs(Received_Msg)
                            test_result(str(Received_Msg),"0")
    ##                        Received_Msg = str(Received_Msg)
    ##                        return Received_Msg

                else:
                            print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                            Rec_Info = "No response from power supply"
                            self.Updating_Logs(Rec_Info)
                            test_result(Rec_Info,"0")
                            #return Rec_Info
                            
                '''
                Selecting the channel
                '''
                Ser_Data = SECOND_CHANNEL.encode()
                self.ser.write(Ser_Data)
                #self.Updating_Logs(SECOND_CHANNEL)
                test_result(str(SECOND_CHANNEL),"0")
                '''
                Getting the channel INFO
                '''
                print("<<<<<<<<<<<<<<Getting the channel INFO>>>>>>>>>>>>>>>>>>>>",CHANNEL_INFO)
                Ser_Data = CHANNEL_INFO.encode()
                self.ser.write(Ser_Data)
                test_result(str(CHANNEL_INFO),"0")
                
                Received_Msg= self.ser.read()
                if Received_Msg:
                                print(Received_Msg)
                                print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
                                self.Updating_Logs(Received_Msg)
                                test_result(str(Received_Msg),"0")
                                
                                #Received_Msg = str(Received_Msg)
                                #return Received_Msg

                else:
                             print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                             Rec_Info = "No response from power supply"
                             self.Updating_Logs(Rec_Info)
                             test_result(Rec_Info,"0")
                             #return Rec_Info
            finally:
                self.lock.release()

            '''
            MSG_FORMAT
            ----------
            '''
                #SETTING TIME AS A MIN TO RUN THE WHILE LOOP
            
            
            
            timeout = time.time() + 60*self.Exicution_Time_Channel_2
            if self.Left_Time_ch2:
                timeout = timeout-self.Left_Time
                print("timeout----timeout---timeout",timeout)
            print("self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1",self.PAUSE_BUTTON_CH_2)
            
            while True:
                                
                                '''
                                seting the channel voltage
                                '''
                                #print("<<<<<<<<<<<<<<> seting the channel voltage>>>>>>>>>>>>>>>>>>>",self.SET_VOLTAGE_ENCO)

                                self.lock.acquire()
                                try:
                                    start_time = time.time()
                                    CURRENT_TIME = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S.%f')
                                    print("<<<<<<<<<<<<<<<<<<<CURRENT_TIME<<<<<<<<<<<<<<<<",CURRENT_TIME)
                                    self.ser.write(self.SET_VOLTAGE_ENCO_CH2)
                                    print("SUCESSFULLY SEND---------------->",self.SET_VOLTAGE_ENCO_CH2)
                                    self.Updating_Logs(self.SET_VOLTAGE_ENCO_CH2)
                                    
                                    
                                    time.sleep(2)
                                    if self.PAUSE_BUTTON_CH_2:
                                        print("<<<<<<RESUMING------------THREAD>>>>>>>")
                                        
                                        break

                                    
                                    if time.time() > timeout:
                                        Time_Out = "Time_Over_CH-1"
                                        #return Time_Out
                                        break
                                    
                                    
                                    '''
                                    Checking-the-current-channel-voltage
                                    '''
                                    
                                    Ser_Data = VOLTAGE_STATUS.encode()
                                    self.ser.write(Ser_Data)
                                    print("SUCESSFULLY SEND---------------->",Ser_Data)
                                    Received_Msg= self.ser.readline()
                                    if Received_Msg:
                                        print(Received_Msg)
                                        print("SUCESSFULLY RECEIVED VOLTAGE_STATUS------->",Received_Msg)
                                        self.Updating_Logs(Received_Msg)
                                        
                                        #Received_Msg = str(Received_Msg)
                                        #return Received_Msg
                                    else:
                                        print(">>>There is No response from power supply<<<<")
                                        Rec_Info = "No response from power supply"
                                        self.Updating_Logs(Received_Msg)
                                        test_result(Rec_Info,"0")

                                    test_result(str(self.SET_VOLTAGE_ENCO_CH2),str(Received_Msg))
                                    
                                finally:
                                    self.lock.release()
                               
                                
                                    #return Rec_Info
##                                time.sleep(2)
##                                
##                                Ser_Data = CURRENT_CHANNEL_ON.encode()
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND--------->",CURRENT_CHANNEL_ON)
##                                Ser_Data = CURRENT_CHANNEL_STATUS.encode()
##                                self.ser.write(Ser_Data)
##
##                                Received_Msg= self.ser.read()
##                                if Received_Msg:
##                                    print(Received_Msg)
##                                    print("SUCESSFULLY RECEIVED------->",Received_Msg)
##                                    #Received_Msg = str(Received_Msg)
##                                    #return Received_Msg
##                                else:
##                                    print(">>>There is No response from power supply<<<")
##                                    Rec_Info = "No response from power supply"
##                                    #return Rec_Info
##                                time.sleep(2)
##
##                                Ser_Data = CURRENT_CHANNEL_OFF.encode()
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND--------->",CURRENT_CHANNEL_OFF)
##                                Ser_Data = CURRENT_CHANNEL_STATUS.encode()
##                                time.sleep(2)
##
##                                
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND--CURRENT_CHANNEL_STATUS------->",CURRENT_CHANNEL_STATUS)
##
##                                Received_Msg= self.ser.read()
##                                if Received_Msg:
##                                    print(Received_Msg)
##                                    print("SUCESSFULLY RECEIVED------->",Received_Msg)
##                                    #Received_Msg = str(Received_Msg)
##                                    #return Received_Msg
##                                else:
##                                    print(">>>There is No response from power supply<<<")
##                                    Rec_Info = "No response from power supply"
##                                    #return Rec_Info
##                                time.sleep(2)

    def  SEND_MSG_CHANNEL_POW_PARAMETER_CH2(self,Iteration_No_Channel_2,On_Time_Channel_2,Off_Time_Channel_2,Voltage_Channel_2):
                if int(Voltage_Channel_2)<0 or int(Voltage_Channel_2) >33 or str(Voltage_Channel_2).isalpha():
                            VALIDATION_FOR_CHANNEL = "####### :-"+"VALIDATION FOR CHANNEL-2 Enter Voltage in 0-32 vtg"
                            print(VALIDATION_FOR_CHANNEL)
                            return VALIDATION_FOR_CHANNEL


                                       
                '''
                CHANNEL VOLTAGE
                '''
                self.Voltage_Channel    = Voltage_Channel_2
                self.SET_VOLTAGE_CH_TWO = SET_VOLTAGE+" "+str(Voltage_Channel_2)+"\n"
                self.SET_VOLTAGE_ENCO_CH2      = self.SET_VOLTAGE_CH_TWO.encode()
                print("GROUP DATA VOLTAGE DATA>>>",self.SET_VOLTAGE_CH_TWO)
                print("ENCODED VOLTAGE DATA",self.SET_VOLTAGE_ENCO_CH2)
                self.Iteration_No_Channel_2 = Iteration_No_Channel_2
                self.On_Time_Channel_2 = On_Time_Channel_2
                self.Off_Time_Channel_2 = Off_Time_Channel_2
                
            
    def SEND_MSG_CHANNEL_POW_CYCLE_CH2(self):

                '''
                Non_Contineous-Mode-OPERATION
                -----------------------------
                '''
                
                self.lock.acquire()
                
                Ser_Data = IDN_SYS_INFO.encode()
                self.ser.write(Ser_Data)
                print("......................SUCESSFULLY SEND------> "+IDN_SYS_INFO)
                Received_Msg= self.ser.readline()
                if Received_Msg:
                            print(Received_Msg)
                            print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
    ##                        Received_Msg = str(Received_Msg)
    ##                        return Received_Msg

                else:
                            print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                            Rec_Info = "No response from power supply"
                            #return Rec_Info

                '''
                Selecting the channel
                '''
                Ser_Data = SECOND_CHANNEL.encode()
                self.ser.write(Ser_Data)
                '''
                Getting the channel INFO
                '''
                print("<<<<<<<<<<<<<<Getting the channel INFO>>>>>>>>>>>>>>>>>>>>",CHANNEL_INFO)
                Ser_Data = CHANNEL_INFO.encode()
                self.ser.write(Ser_Data)
                Received_Msg= self.ser.read()
                
                if Received_Msg:
                                print(Received_Msg)
                                print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
                                #Received_Msg = str(Received_Msg)
                                #return Received_Msg

                else:
                             print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                             Rec_Info = "No response from power supply"
                             #return Rec_Info

                self.lock.release()

                             
                Count = 0

                print(">>>>>>>>>>>>>NON_CONTINEOUS MODE<<<<<<<<<<<",self.SET_VOLTAGE_ENCO_CH2)
                while Count< int(self.Iteration_No_Channel_2):
                            self.stop_threads = False
                            Power_Cycle = threading.Thread(target = self.Non_Contineous_Mode_Channel_2) 
                            Power_Cycle.start() 
                            time.sleep(int(self.On_Time_Channel_2)) 
                            self.stop_threads = True
                            Power_Cycle.join() 
                            print('thread killed')
                            Count = Count+1
                            print("Iteration_No_Channel_1",Count)
                            time.sleep(int(self.Off_Time_Channel_2))
                            


    def Non_Contineous_Mode_Channel_2(self):
              
                while True:
                    if self.stop_threads: 
                        break
                    
                    self.lock.acquire()
                    start_time = time.time()
                    CURRENT_TIME = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S.%f')
                    print("<<<<<<<<<<<<<<<<<<<CURRENT_TIME<<<<<<<<<<<<<<<<",CURRENT_TIME)
                    self.ser.write(self.SET_VOLTAGE_ENCO_CH2)
                    print("SUCESSFULLY SEND---------------->",self.SET_VOLTAGE_ENCO_CH2)
                    print("INSIDE THE Non_Contineous_Mode_Channel_1")
                    

                    '''
                    Checking the current channel voltage
                    '''
                                        
                    self.Ser_Data = VOLTAGE_STATUS.encode()
                    self.ser.write(self.Ser_Data)
                    print("SUCESSFULLY SEND---------------->",self.Ser_Data)
                    Received_Msg= self.ser.readline()
                    if Received_Msg:
                                print(Received_Msg)
                                print("SUCESSFULLY RECEIVED VOLTAGE_STATUS------->",Received_Msg)
                                            #Received_Msg = str(Received_Msg)
                                            #resturn Received_Msg
                    else:
                                print(">>>There is No response from power supply<<<<")
                                Rec_Info = "No response from power supply"

                    self.lock.release()



        #################################################### CHANNEL-THREE ############################################################################

                                
    def SEND_MSG_CHANNEL_THREE_PARAMETERS(self,Voltage_Channel_3,Exicution_Time_Channel_3):
        """
            This function create the message format
            Parameters
            ----------
            arg : int

            Returns
            ----------
            bytes
                Message that is sended through com port.

            CURRENT-AND-VOLTAGE-VALIDATION FOR CHANNEL-1
            Channel 1 maximum voltage = 100
            minimum = 0
            
            -------------------------------------------
        """

        if int(Voltage_Channel_3)<0 or int(Voltage_Channel_3) >33 or str(Voltage_Channel_3).isalpha():
            
            VALIDATION_FOR_CHANNEL = "####### :-"+"VALIDATION FOR CHANNEL-3 Enter Voltage in 0-32 vtg"
            print(VALIDATION_FOR_CHANNEL)
            return VALIDATION_FOR_CHANNEL


        ##            
##            if int(Current_Channel_1)<0 or int(Current_Channel_1) >3 or str(Current_Channel_1).isalpha():
##                print("Enter Valied Current in between 0-3 Amp")
##                VALIDATION_FOR_CHANNEL = "####### :-"+"VALIDATION FOR CHANNEL-1 Current in between 0-3 Amp"
##                print(VALIDATION_FOR_CHANNEL)
##                return VALIDATION_FOR_CHANNEL
                

                               
        '''
        CHANNEL VOLTAGE
        '''
        self.Voltage_Channel            = Voltage_Channel_3
        self.SET_VOLTAGE_CH_THREE       = SET_VOLTAGE+" "+str(Voltage_Channel_3)+"\n"
        self.SET_VOLTAGE_ENCO_THREE     = self.SET_VOLTAGE_CH_THREE.encode()
        self.Exicution_Time_Channel_3   = Exicution_Time_Channel_3
        print("GROUP DATA VOLTAGE DATA>>>",self.SET_VOLTAGE_CH_THREE)
        print("ENCODED VOLTAGE DATA",self.SET_VOLTAGE_ENCO_THREE)


        
            
    def UPDATE_FLAG_PAUSE_CH3(self):
        self.PAUSE_BUTTON_CH_3 = True

        
    def UPDATE_FLAG_RESUME_CH3(self):
        self.PAUSE_BUTTON_CH_3 = False
        
    def UPDATE_FLAG_SEND_CH3(self):
         self.PAUSE_BUTTON_CH_3 = False
        

    def Update_Pause_Time_CH3(self,PAUSE_TIME):
        self.Pause_Time_Three = PAUSE_TIME
        print("Update_Resume_Time",self.Pause_Time_Three)
        
    def Updated_Start_Time_CH3(self,START_TIME):
        self.Start_Time_Three = START_TIME
        print("Updated_Start_Time", self.Start_Time_Three)
        
    
    def Update_Time_Ch3(self):
        print("Inside the Update Time")
        
        self.Left_Time_Ch3 = self.Pause_Time_Three- self.Start_Time_Three
        print("Update_Time---Update_Time---Update_Time--Update_Time",self.Left_Time_Ch3)
        
                    
    def SEND_MSG_CHANNEL_THREE_CON(self):
##            print("Left_TimeLeft_TimeLeft_Time",self.Left_Time)
##            
##            timeout = time.time() + 60*self.Exicution_Time_Channel_1
##            if self.Left_Time:
##                timeout = timeout-self.Left_Time
##                print("timeout----timeout---timeout",timeout) 

            
##            while True:
##                if self.PAUSE_BUTTON_CH_1:
##                    break
##
##                if time.time() > timeout:
##                    Time_Out = "Time_Over_CH-1"
##                                    #return Time_Out
##                    break
##                
##                time.sleep(1)
##                print("!!!!!!!!!!!!!!!!!!SEND_MSG_CHANNEL_ONE_CON!!!!!!!!!!!!!!!!!!!!")
            #print("self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1",self.PAUSE_BUTTON_CH_1)

            #else:
            #print("<<<<<<<<<<<<<<IDN_SYS_INFO INFO>>>>>>>>>>>>>>>>>>>>",IDN_SYS_INFO)
            self.lock.acquire()
            try:
                Ser_Data = IDN_SYS_INFO.encode()
                self.ser.write(Ser_Data)
                test_result(str(IDN_SYS_INFO),"0")
                print("......................SUCESSFULLY SEND------> "+IDN_SYS_INFO)
                self.Updating_Logs(IDN_SYS_INFO)
                Received_Msg= self.ser.readline()
                
                if Received_Msg:
                            print(Received_Msg)
                            print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
                            self.Updating_Logs(Received_Msg)
                            test_result(str(Received_Msg),"0")
    ##                        Received_Msg = str(Received_Msg)
    ##                        return Received_Msg

                else:
                            print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                            Rec_Info = "No response from power supply"
                            self.Updating_Logs(Rec_Info)
                            test_result(Rec_Info,"0")
                            #return Rec_Info
                            
                '''
                Selecting the channel
                '''
                Ser_Data = THIRD_CHANNEL.encode()
                self.ser.write(Ser_Data)
                #self.Updating_Logs(FIRST_CHANNEL)
                test_result(str(THIRD_CHANNEL),"0")
                '''
                Getting the channel INFO
                '''
                print("<<<<<<<<<<<<<<Getting the channel INFO>>>>>>>>>>>>>>>>>>>>",CHANNEL_INFO)
                Ser_Data = CHANNEL_INFO.encode()
                self.ser.write(Ser_Data)
                test_result(str(CHANNEL_INFO),"0")
                
                Received_Msg= self.ser.read()
                if Received_Msg:
                                print(Received_Msg)
                                print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
                                self.Updating_Logs(Received_Msg)
                                test_result(str(Received_Msg),"0")
                                
                                #Received_Msg = str(Received_Msg)
                                #return Received_Msg

                else:
                             print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                             Rec_Info = "No response from power supply"
                             self.Updating_Logs(Rec_Info)
                             test_result(Rec_Info,"0")
                             #return Rec_Info
            finally:
                self.lock.release()

            '''
            MSG_FORMAT
            ----------
            '''
                #SETTING TIME AS A MIN TO RUN THE WHILE LOOP
            
            print("Left_TimeLeft_TimeLeft_Time",self.Left_Time)
            
            timeout = time.time() + 60*self.Exicution_Time_Channel_3
            if self.Left_Time_Ch3:
                timeout = timeout-self.Left_Time
                print("timeout----timeout---timeout",timeout)
                
            while True:
                                print("self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1self.PAUSE_BUTTON_CH_1",self.PAUSE_BUTTON_CH_1)
                                '''
                                seting the channel voltage
                                '''
                                #print("<<<<<<<<<<<<<<> seting the channel voltage>>>>>>>>>>>>>>>>>>>",self.SET_VOLTAGE_ENCO)
                                self.lock.acquire()
                                try:
                                    start_time = time.time()
                                    CURRENT_TIME = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S.%f')
                                    print("<<<<<<<<<<<<<<<<<<<CURRENT_TIME<<<<<<<<<<<<<<<<",CURRENT_TIME)
                                    self.ser.write(self.SET_VOLTAGE_ENCO_THREE)
                                    print("SUCESSFULLY SEND---------------->",self.SET_VOLTAGE_ENCO_THREE)
                                    self.Updating_Logs(self.SET_VOLTAGE_ENCO_THREE)
                                    
                                    
                                    time.sleep(2)
                                    if self.PAUSE_BUTTON_CH_3:
                                        print("<<<<<<RESUMING------------THREAD>>>>>>>")
                                        
                                        break

                                    
                                    if time.time() > timeout:
                                        Time_Out = "Time_Over_CH-2"
                                        #return Time_Out
                                        break


                                    '''
                                    Checking the current channel voltage
                                    '''
                                    
                                    Ser_Data = VOLTAGE_STATUS.encode()
                                    self.ser.write(Ser_Data)
                                    print("SUCESSFULLY SEND---------------->",Ser_Data)
                                    Received_Msg= self.ser.readline()
                                    if Received_Msg:
                                        print(Received_Msg)
                                        print("SUCESSFULLY RECEIVED VOLTAGE_STATUS------->",Received_Msg)
                                        self.Updating_Logs(Received_Msg)
                                        
                                        #Received_Msg = str(Received_Msg)
                                        #return Received_Msg
                                    else:
                                        print(">>>There is No response from power supply<<<<")
                                        Rec_Info = "No response from power supply"
                                        self.Updating_Logs(Received_Msg)
                                        test_result(Rec_Info,"0")

                                    test_result(str(self.SET_VOLTAGE_ENCO_THREE),str(Received_Msg))
                                finally:
                                    self.lock.release()
                                
                                    #return Rec_Info
##                                time.sleep(2)
##                                
##                                Ser_Data = CURRENT_CHANNEL_ON.encode()
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND--------->",CURRENT_CHANNEL_ON)
##                                Ser_Data = CURRENT_CHANNEL_STATUS.encode()
##                                self.ser.write(Ser_Data)
##
##                                Received_Msg= self.ser.read()
##                                if Received_Msg:
##                                    print(Received_Msg)
##                                    print("SUCESSFULLY RECEIVED------->",Received_Msg)
##                                    #Received_Msg = str(Received_Msg)
##                                    #return Received_Msg
##                                else:
##                                    print(">>>There is No response from power supply<<<")
##                                    Rec_Info = "No response from power supply"
##                                    #return Rec_Info
##                                time.sleep(2)
##
##                                Ser_Data = CURRENT_CHANNEL_OFF.encode()
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND--------->",CURRENT_CHANNEL_OFF)
##                                Ser_Data = CURRENT_CHANNEL_STATUS.encode()
##                                time.sleep(2)
##
##                                
##                                self.ser.write(Ser_Data)
##                                print("SUCESSFULLY SEND--CURRENT_CHANNEL_STATUS------->",CURRENT_CHANNEL_STATUS)
##
##                                Received_Msg= self.ser.read()
##                                if Received_Msg:
##                                    print(Received_Msg)
##                                    print("SUCESSFULLY RECEIVED------->",Received_Msg)
##                                    #Received_Msg = str(Received_Msg)
##                                    #return Received_Msg
##                                else:
##                                    print(">>>There is No response from power supply<<<")
##                                    Rec_Info = "No response from power supply"
##                                    #return Rec_Info
##                                time.sleep(2)

    def  SEND_MSG_CHANNEL_POW_PARAMETER_THREE(self,Iteration_No_Channel_3,On_Time_Channel_3,Off_Time_Channel_3,Voltage_Channel_3):
                if int(Voltage_Channel_3)<0 or int(Voltage_Channel_3) >33 or str(Voltage_Channel_3).isalpha():
                            VALIDATION_FOR_CHANNEL = "####### :-"+"VALIDATION FOR CHANNEL-1 Enter Voltage in 0-32 vtg"
                            print(VALIDATION_FOR_CHANNEL)
                            return VALIDATION_FOR_CHANNEL


                                       
                '''
                CHANNEL VOLTAGE
                '''
                self.Voltage_Channel            = Voltage_Channel_3
                self.SET_VOLTAGE_CH_THREE       = SET_VOLTAGE+" "+str(Voltage_Channel_3)+"\n"
                self.SET_VOLTAGE_ENCO_THREE     = self.SET_VOLTAGE_CH_THREE.encode()
                self.Iteration_No_Channel_3     = Iteration_No_Channel_3
                self.On_Time_Channel_3          = On_Time_Channel_3
                self.Off_Time_Channel_3         = Off_Time_Channel_3
                print("GROUP DATA VOLTAGE DATA>>>",self.SET_VOLTAGE_CH_THREE)
                print("ENCODED VOLTAGE DATA",self.SET_VOLTAGE_ENCO_THREE)
                
            
    def SEND_MSG_CHANNEL_POW_CYCLE_CH3(self):

                '''
                Non_Contineous-Mode-OPERATION
                -----------------------------
                '''
                self.lock.acquire()

                Ser_Data = IDN_SYS_INFO.encode()
                self.ser.write(Ser_Data)
                print("......................SUCESSFULLY SEND------> "+IDN_SYS_INFO)
                Received_Msg= self.ser.readline()
                if Received_Msg:
                            print(Received_Msg)
                            print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
    ##                        Received_Msg = str(Received_Msg)
    ##                        return Received_Msg

                else:
                            print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                            Rec_Info = "No response from power supply"
                            #return Rec_Info

                '''
                Selecting the channel
                '''
                Ser_Data = THIRD_CHANNEL.encode()
                self.ser.write(Ser_Data)
                '''
                Getting the channel INFO
                '''
                print("<<<<<<<<<<<<<<Getting the channel INFO>>>>>>>>>>>>>>>>>>>>",CHANNEL_INFO)
                Ser_Data = CHANNEL_INFO.encode()
                self.ser.write(Ser_Data)
                Received_Msg= self.ser.read()
                
                if Received_Msg:
                                print(Received_Msg)
                                print("SUCESSFULLY RECEIVED-------------->",Received_Msg)
                                #Received_Msg = str(Received_Msg)
                                #return Received_Msg

                else:
                             print(">>>>>>>>>>>>>>>>>There is No response from power supply<<<<<<<<<<<<<<<<<<")
                             Rec_Info = "No response from power supply"
                             #return Rec_Info

                self.lock.release()

                             
                Count = 0

                print(">>>>>>>>>>>>>NON_CONTINEOUS MODE<<<<<<<<<<<",self.SET_VOLTAGE_ENCO_THREE)
                while Count< int(self.Iteration_No_Channel_3):
                            self.stop_threads = False
                            Power_Cycle = threading.Thread(target = self.Non_Contineous_Mode_Channel_3) 
                            Power_Cycle.start() 
                            time.sleep(int(self.On_Time_Channel_3)) 
                            self.stop_threads = True
                            Power_Cycle.join() 
                            print('thread killed')
                            Count = Count+1
                            print("Iteration_No_Channel_3",Count)
                            time.sleep(int(self.Off_Time_Channel_3))



    def Non_Contineous_Mode_Channel_3(self):
                while True:
                    if self.stop_threads: 
                        break
                    
                    start_time = time.time()
                    CURRENT_TIME = datetime.datetime.fromtimestamp(start_time).strftime('%H:%M:%S.%f')
                    print("<<<<<<<<<<<<<<<<<<<CURRENT_TIME<<<<<<<<<<<<<<<<",CURRENT_TIME)
                    self.lock.acquire()
                    self.ser.write(self.SET_VOLTAGE_ENCO_THREE)
                    print("SUCESSFULLY SEND---------------->",self.SET_VOLTAGE_ENCO_THREE)
                    print("INSIDE THE Non_Contineous_Mode_Channel_1")
                    

                    '''
                    Checking the current channel voltage
                    '''
                                        
                    self.Ser_Data = VOLTAGE_STATUS.encode()
                    self.ser.write(self.Ser_Data)
                    print("SUCESSFULLY SEND---------------->",self.Ser_Data)
                    Received_Msg= self.ser.readline()
                    if Received_Msg:
                                print(Received_Msg)
                                print("SUCESSFULLY RECEIVED VOLTAGE_STATUS------->",Received_Msg)
                                            #Received_Msg = str(Received_Msg)
                                            #resturn Received_Msg
                    else:
                                print(">>>There is No response from power supply<<<<")
                                Rec_Info = "No response from power supply"

                    self.lock.release()

                                

    def Updating_Logs(self,u32UPDATED_LOG):
        window = Ui_MainWindow()
        #print("Updating_Logs--Updating_Logs---Updating_Logs---Updating_Logs---Updating_Logs")

        print(u32UPDATED_LOG)

##        print("Updating_Logs--Updating_Logs---Updating_Logs---Updating_Logs---Updating_Logs")
##        window.UPDATED_LOG(UPDATED_LOG)

        
        
        
        

        
    


        
                                
                       
                                    

                    
        
