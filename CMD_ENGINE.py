from Communication_Api import*
Channel1_Mode = []
Channel2_Mode = []
Channel3_Mode = []


def CMD_POWER_ENGINE():
        print("<<<<<<<<<<<<<<CMD>>>>>>>>>>>>>>>>>>")
        try:
            from configparser import ConfigParser
        except ImportError:
            from ConfigParser import ConfigParser  # ver. < 3.0


        config = ConfigParser()
        config.read('Config.ini')

        '''
        COM-PORT CONFIGURATION
        ----------------------
        '''
        Baudrat=config.get('COM_PORT_CONFIGURATION', 'Baudrate')
        print(Baudrat)
        COM_PORT=config.get('COM_PORT_CONFIGURATION', 'COM_PORT')
        print(COM_PORT)
        
        if Baudrat:
            print("COM-PORT-BAUDRATE")
        else:
            Baudrat = 115200#Default baudrate

        if COM_PORT == "":
            print("<<<<<<<<<<ENTER THE VALIED COM PORT>>>>>>>>")
        
        '''
        Channel-1 INPUT DATA
        --------------------
        '''
        Channel_1_Voltage=config.get('CHANNEL-1', 'Channel_1_Voltage')
        print(Channel_1_Voltage)
        if Channel_1_Voltage:
            print(Channel_1_Voltage)
        else:
            #setting default Voltage
            Channel_1_Voltage = 14
            print(Channel_1_Voltage)
            
        Channel_1_Current=config.get('CHANNEL-1', 'Channel_1_Current')
        print(Channel_1_Current)
        if Channel_1_Current:
            print(Channel_1_Current)
        else:
            #setting default Current
            Channel_1_Current = 2
            print(Channel_1_Current)
        
            
        Mode_Of_Operation_1=config.get('CHANNEL-1', 'Mode_Of_Operation_1')
        print(Mode_Of_Operation_1)
        
        Mode_Of_Operation_2=config.get('CHANNEL-1', 'Mode_Of_Operation_2')
        print(Mode_Of_Operation_2)

        if Mode_Of_Operation_1 == "Contineous_Mode":
            Mode_Of_Operation_Channel_1  = Mode_Of_Operation_1
            TIME=config.get('CHANNEL-1', 'TIME')
            #Channel1_Mode.append(TIME)
            Con_Time_Channel_1 = TIME


            Channel1_Mode.append(Mode_Of_Operation_1)
            Channel1_Mode.append(Channel_1_Voltage)
            Channel1_Mode.append(Con_Time_Channel_1)

            
            
        elif Mode_Of_Operation_2 == "Non_Contineous_Mode":
            #Channel1_Mode.append(Mode_Of_Operation_2)
            Mode_Of_Operation_Channel_1 = Mode_Of_Operation_2
            ON_TIME=config.get('CHANNEL-1', 'ON_TIME ')
            ON_TIME_Channel_1 = ON_TIME
            #print(ON_TIME)
            Channel1_Mode.append(ON_TIME)
            OFF_TIME =config.get('CHANNEL-1', 'OFF_TIME ')
            Channel1_Mode.append(OFF_TIME)
            OFF_TIME_Channel_1 =OFF_TIME
            #print(OFF_TIME)
            ITERATION_NO=config.get('CHANNEL-1', 'ITERATION_NO')
            #print(ITERATION_NO)
            Channel1_Mode.append(ITERATION_NO)
        
            
            
      
        

        '''
        Channel-2 INPUT DATA
        --------------------
        '''
        Channel_2_Voltage=config.get('CHANNEL-2', 'Channel_2_Voltage')
        #print(Channel_2_Voltage)
        if Channel_2_Voltage:
            print("Channel_2_Voltage",Channel_2_Voltage)
        else:
            #setting default Voltage
            Channel_2_Voltage = 14


        Channel_2_Current=config.get('CHANNEL-2', 'Channel_2_Current')
        print("Channel_2_Current",Channel_2_Current)
        if Channel_2_Current:
            print(Channel_2_Current)
        else:
            #setting default Current
            Channel_2_Current = 2

        Mode_Of_Operation_1=config.get('CHANNEL-2', 'Mode_Of_Operation_1')

        print(">>>>>>>>>>>>>>>>>>>>>>Before the if statement in channel 2")
        if Mode_Of_Operation_1 == "Contineous-Mode":
            TIME=config.get('CHANNEL-2', 'TIME')
            Channel2_Mode.apend(Mode_Of_Operation_1)
            Con_Time_Channel_2 =TIME 
            print(">>>>>>>>>>>>>>>>>>>>>>Inside the if statement in channel 2")
            Channel2_Mode.append(Mode_Of_Operation_1)
            Channel2_Mode.append(Channel_2_Voltage)
            Channel2_Mode.append(Con_Time_Channel_2)

            
        elif Mode_Of_Operation_1 == "Non_Contineous_Mode":
            print(">>>>>>>>>>>>>>>>>>>>>>Inside the elif statement in channel 2")
            #ON_TIME=config.get('CHANNEL-2', 'ON_TIME ')
            #print(ON_TIME)
            #Channel2_Mode.apend(Mode_Of_Operation_1)
            #Channel2_Mode.apend(ON_TIME)
            #OFF_TIME =config.get('CHANNEL-2', 'OFF_TIME ')
            #print(OFF_TIME)
            Channel2_Mode.apend(OFF_TIME)
            
            ITERATION_NO=config.get('CHANNEL-2', 'ITERATION_NO')
            #print(ITERATION_NO)
            Channel2_Mode.apend(ITERATION_NO)
        
            


        '''
        Channel-3 INPUT DATA
        --------------------
        '''
        Channel_3_Voltage=config.get('CHANNEL-3', 'Channel_3_Voltage')
        #print(Channel_3_Voltage)
        if Channel_3_Voltage:
            print(Channel_3_Voltage)
        else:
            #setting default Voltage
            Channel_3_Voltage = 14

            
        Channel_3_Current=config.get('CHANNEL-3', 'Channel_3_Current')
        #print(Channel_3_Current)
        if Channel_3_Current:
            print(Channel_3_Current)
        else:
            #setting default Current
            Channel_3_Current = 2
        
        Mode_Of_Operation_1=config.get('CHANNEL-3', 'Mode_Of_Operation_1')
        #print(Mode_Of_Operation_1)
        
        Mode_Of_Operation_2=config.get('CHANNEL-3', 'Mode_Of_Operation_2')
        #print(Mode_Of_Operation_2)


        
        if Mode_Of_Operation_1 == "Contineous_Mode":
            TIME=config.get('CHANNEL-3', 'TIME')

            Con_Time_Channel_3 = TIME
            Channel3_Mode.append(Mode_Of_Operation_1)
            Channel3_Mode.append(Channel_3_Voltage)
            Channel3_Mode.append(Con_Time_Channel_3)

            
            
        elif Mode_Of_Operation_1 == "Non_Contineous_Mode":
            ON_TIME=config.get('CHANNEL-3', 'ON_TIME ')
            #print(ON_TIME)
            Channel3_Mode.apend(Mode_Of_Operation_1)
            Channel3_Mode.apend(ON_TIME)
            OFF_TIME =config.get('CHANNEL-3', 'OFF_TIME ')
            #print(OFF_TIME)
            Channel3_Mode.apend(OFF_TIME)
            ITERATION_NO=config.get('CHANNEL-3', 'ITERATION_NO')
            #print(ITERATION_NO)
            Channel3_Mode.apend(ITERATION_NO)
        
        print("Channel_1_Voltage",Channel1_Mode)
        print("Channel_2_Voltage",Channel2_Mode)
        print("Channel_3_Voltage",Channel3_Mode)
        
        #print("OFF_TIME_Channel_1",OFF_TIME_Channel_1)
        #print("ON_TIME_Channel_1",ON_TIME_Channel_1)
        #print("Con_Time_Channel_1",Con_Time_Channel_1)
        #print("Mode_Of_Operation_Channel_1",Mode_Of_Operation_Channel_1)
        #window.COM_PORT(COM_PORT,Baudrat)
        #print("Channel1_Mode",Channel1_Mode)
        #window.SEND_MSG_CHANNEL_1(Channel1_Mode)
        



