# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QSplashScreen
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import sys
from PyQt5.QtGui import QIcon, QPixmap
from datetime import datetime
import sys
import serial.tools.list_ports
import serial
#from serial import SerialException
import threading
import time
import datetime
global stop_threads
from Communication_Api import*
from CMD_ENGINE import*
START_BUTTON = False
PAUSE_BUTTON = False
RESUME_BUTTON = False
global Loop_Time

COMMND_LIST = []
now_1 = datetime.datetime.now()
class Ui_MainWindow(object):
    def __init__(self):
        self.PillKillThreadOne = threading.Event()
        self.PillKillThreadTwo = threading.Event()
        self.PillKillThreadThree = threading.Event()
        self.stop_threads = False
        self.SEND_BUTTON = False
        self.Voltage_Channel_1 = 1
        self.Exicution_Time_Channel_1 = 1
        self.Iteration_No_Channel_1 = 1
        self.On_Time_Channel_1 = 1
        self.Off_Time_Channel_1 = 1
        self.START = 0


        
    def setupUi(self, MainWindow):
        self.Communication_Obj = Communication_Api()
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1115, 450)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralWidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 20, 751, 101))
        self.groupBox.setObjectName("groupBox")
        self.lineEdit_Voltage_Channel_1 = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_Voltage_Channel_1.setGeometry(QtCore.QRect(50, 30, 41, 20))
        self.lineEdit_Voltage_Channel_1.setObjectName("lineEdit_Voltage_Channel_1")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(10, 30, 41, 21))
        self.label.setObjectName("label")
        self.groupBox_6 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_6.setGeometry(QtCore.QRect(410, 20, 271, 71))
        self.groupBox_6.setObjectName("groupBox_6")
        self.radioButton_Power_Cycle_Channel_1 = QtWidgets.QRadioButton(self.groupBox_6)
        self.radioButton_Power_Cycle_Channel_1.setGeometry(QtCore.QRect(200, 40, 71, 21))
        self.radioButton_Power_Cycle_Channel_1.setObjectName("radioButton_Power_Cycle_Channel_1")
        self.lineEdit_Off_Time_Channel_1 = QtWidgets.QLineEdit(self.groupBox_6)
        self.lineEdit_Off_Time_Channel_1.setGeometry(QtCore.QRect(150, 40, 41, 20))
        self.lineEdit_Off_Time_Channel_1.setObjectName("lineEdit_Off_Time_Channel_1")
        self.label_16 = QtWidgets.QLabel(self.groupBox_6)
        self.label_16.setGeometry(QtCore.QRect(150, 20, 41, 20))
        self.label_16.setObjectName("label_16")
        self.label_15 = QtWidgets.QLabel(self.groupBox_6)
        self.label_15.setGeometry(QtCore.QRect(90, 20, 41, 20))
        self.label_15.setObjectName("label_15")
        self.lineEdit_On_Time_Channel_1 = QtWidgets.QLineEdit(self.groupBox_6)
        self.lineEdit_On_Time_Channel_1.setGeometry(QtCore.QRect(90, 40, 41, 20))
        self.lineEdit_On_Time_Channel_1.setObjectName("lineEdit_On_Time_Channel_1")
        self.label_5 = QtWidgets.QLabel(self.groupBox_6)
        self.label_5.setGeometry(QtCore.QRect(10, 20, 61, 20))
        self.label_5.setObjectName("label_5")
        self.lineEdit_Iteration_No_Channel_1 = QtWidgets.QLineEdit(self.groupBox_6)
        self.lineEdit_Iteration_No_Channel_1.setGeometry(QtCore.QRect(10, 40, 41, 20))
        self.lineEdit_Iteration_No_Channel_1.setObjectName("lineEdit_Iteration_No_Channel_1")
        self.groupBox_7 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_7.setGeometry(QtCore.QRect(100, 20, 301, 71))
        self.groupBox_7.setObjectName("groupBox_7")
        self.lineEdit_Time_Contineous_Channel_1 = QtWidgets.QLineEdit(self.groupBox_7)
        self.lineEdit_Time_Contineous_Channel_1.setGeometry(QtCore.QRect(10, 40, 41, 20))
        self.lineEdit_Time_Contineous_Channel_1.setObjectName("lineEdit_Time_Contineous_Channel_1")
        self.label_4 = QtWidgets.QLabel(self.groupBox_7)
        self.label_4.setGeometry(QtCore.QRect(10, 20, 81, 16))
        self.label_4.setObjectName("label_4")
        self.RESUME_Channel_1 = QtWidgets.QPushButton(self.groupBox_7)
        self.RESUME_Channel_1.setGeometry(QtCore.QRect(190, 30, 31, 31))
        self.RESUME_Channel_1.setMinimumSize(QtCore.QSize(21, 0))
        self.RESUME_Channel_1.setStyleSheet("")
        self.RESUME_Channel_1.setObjectName("RESUME_Channel_1")
        self.radioButton_Contineous_Channel_1 = QtWidgets.QRadioButton(self.groupBox_7)
        self.radioButton_Contineous_Channel_1.setGeometry(QtCore.QRect(230, 40, 71, 20))
        self.radioButton_Contineous_Channel_1.setObjectName("radioButton_Contineous_Channel_1")
        self.PAUSE_Channel_1 = QtWidgets.QPushButton(self.groupBox_7)
        self.PAUSE_Channel_1.setGeometry(QtCore.QRect(110, 30, 31, 31))
        self.PAUSE_Channel_1.setStyleSheet("")
        self.PAUSE_Channel_1.setObjectName("PAUSE_Channel_1")
        self.START_Channel_1 = QtWidgets.QPushButton(self.groupBox)
        self.START_Channel_1.setGeometry(QtCore.QRect(690, 40, 41, 31))
        self.START_Channel_1.setStyleSheet("")
        self.START_Channel_1.setObjectName("START_Channel_1")
        self.lineEdit_Current_Channel_1 = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_Current_Channel_1.setGeometry(QtCore.QRect(50, 70, 41, 20))
        self.lineEdit_Current_Channel_1.setObjectName("lineEdit_Current_Channel_1")
        self.label_8 = QtWidgets.QLabel(self.groupBox)
        self.label_8.setGeometry(QtCore.QRect(10, 70, 41, 21))
        self.label_8.setObjectName("label_8")
        self.groupBox_4 = QtWidgets.QGroupBox(self.centralWidget)
        self.groupBox_4.setGeometry(QtCore.QRect(770, 20, 331, 331))
        self.groupBox_4.setObjectName("groupBox_4")
        self.Com_Port_Box = QtWidgets.QComboBox(self.groupBox_4)
        self.Com_Port_Box.setGeometry(QtCore.QRect(230, 20, 71, 22))
        self.Com_Port_Box.setObjectName("Com_Port_Box")
        self.OPEN_COM_PORT = QtWidgets.QPushButton(self.groupBox_4)
        self.OPEN_COM_PORT.setGeometry(QtCore.QRect(230, 50, 61, 21))
        self.OPEN_COM_PORT.setObjectName("OPEN_COM_PORT")
        self.SEND = QtWidgets.QPushButton(self.groupBox_4)
        self.SEND.setGeometry(QtCore.QRect(230, 100, 61, 21))
        self.SEND.setObjectName("SEND")
        self.CLOSE = QtWidgets.QPushButton(self.groupBox_4)
        self.CLOSE.setGeometry(QtCore.QRect(230, 130, 61, 20))
        self.CLOSE.setObjectName("CLOSE")
        self.label_7 = QtWidgets.QLabel(self.groupBox_4)
        self.label_7.setGeometry(QtCore.QRect(200, 20, 31, 16))
        self.label_7.setObjectName("label_7")
        self.label_Connected = QtWidgets.QLabel(self.groupBox_4)
        self.label_Connected.setGeometry(QtCore.QRect(10, 130, 47, 13))
        self.label_Connected.setText("")
        self.label_Connected.setObjectName("label_Connected")
        self.groupBox_5 = QtWidgets.QGroupBox(self.groupBox_4)
        self.groupBox_5.setGeometry(QtCore.QRect(10, 20, 171, 41))
        self.groupBox_5.setTitle("")
        self.groupBox_5.setObjectName("groupBox_5")
        self.label_Baudrate = QtWidgets.QLabel(self.groupBox_5)
        self.label_Baudrate.setGeometry(QtCore.QRect(10, 10, 51, 20))
        self.label_Baudrate.setObjectName("label_Baudrate")
        self.lineEdit_Baudrate = QtWidgets.QLineEdit(self.groupBox_5)
        self.lineEdit_Baudrate.setGeometry(QtCore.QRect(80, 10, 81, 20))
        self.lineEdit_Baudrate.setObjectName("lineEdit_Baudrate")
        self.Com_Open_label = QtWidgets.QLabel(self.groupBox_4)
        self.Com_Open_label.setGeometry(QtCore.QRect(230, 80, 91, 16))
        self.Com_Open_label.setText("")
        self.Com_Open_label.setObjectName("Com_Open_label")
        self.Com_Close_Label = QtWidgets.QLabel(self.groupBox_4)
        self.Com_Close_Label.setGeometry(QtCore.QRect(220, 150, 81, 16))
        self.Com_Close_Label.setText("")
        self.Com_Close_Label.setObjectName("Com_Close_Label")
        self.TextEdit_Tx_Data = QtWidgets.QTextEdit(self.groupBox_4)
        self.TextEdit_Tx_Data.setGeometry(QtCore.QRect(20, 160, 301, 161))
        self.TextEdit_Tx_Data.setObjectName("TextEdit_Tx_Data")
        self.label_3 = QtWidgets.QLabel(self.groupBox_4)
        self.label_3.setGeometry(QtCore.QRect(20, 130, 61, 31))
        self.label_3.setObjectName("label_3")
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralWidget)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 130, 751, 101))
        self.groupBox_2.setObjectName("groupBox_2")
        self.ineEdit_Voltage_Channel_2 = QtWidgets.QLineEdit(self.groupBox_2)
        self.ineEdit_Voltage_Channel_2.setGeometry(QtCore.QRect(50, 30, 41, 20))
        self.ineEdit_Voltage_Channel_2.setObjectName("ineEdit_Voltage_Channel_2")
        self.label_6 = QtWidgets.QLabel(self.groupBox_2)
        self.label_6.setGeometry(QtCore.QRect(10, 30, 41, 21))
        self.label_6.setObjectName("label_6")
        self.groupBox_8 = QtWidgets.QGroupBox(self.groupBox_2)
        self.groupBox_8.setGeometry(QtCore.QRect(100, 20, 301, 71))
        self.groupBox_8.setObjectName("groupBox_8")
        self.lineEdit_Time_Contineous_Channel_2 = QtWidgets.QLineEdit(self.groupBox_8)
        self.lineEdit_Time_Contineous_Channel_2.setGeometry(QtCore.QRect(10, 40, 41, 20))
        self.lineEdit_Time_Contineous_Channel_2.setObjectName("lineEdit_Time_Contineous_Channel_2")
        self.label_9 = QtWidgets.QLabel(self.groupBox_8)
        self.label_9.setGeometry(QtCore.QRect(10, 20, 81, 16))
        self.label_9.setObjectName("label_9")
        self.PAUSE_Channel_2 = QtWidgets.QPushButton(self.groupBox_8)
        self.PAUSE_Channel_2.setGeometry(QtCore.QRect(110, 30, 31, 31))
        self.PAUSE_Channel_2.setStyleSheet("")
        self.PAUSE_Channel_2.setObjectName("PAUSE_Channel_2")
        self.RESUME_Channel_2 = QtWidgets.QPushButton(self.groupBox_8)
        self.RESUME_Channel_2.setGeometry(QtCore.QRect(190, 30, 31, 31))
        self.RESUME_Channel_2.setStyleSheet("")
        self.RESUME_Channel_2.setObjectName("RESUME_Channel_2")
        self.radioButton_Contineous_Channel_2 = QtWidgets.QRadioButton(self.groupBox_8)
        self.radioButton_Contineous_Channel_2.setGeometry(QtCore.QRect(230, 40, 71, 20))
        self.radioButton_Contineous_Channel_2.setObjectName("radioButton_Contineous_Channel_2")
        self.groupBox_9 = QtWidgets.QGroupBox(self.groupBox_2)
        self.groupBox_9.setGeometry(QtCore.QRect(410, 20, 271, 71))
        self.groupBox_9.setObjectName("groupBox_9")
        self.lineEdit_Iteration_No_Channel_2 = QtWidgets.QLineEdit(self.groupBox_9)
        self.lineEdit_Iteration_No_Channel_2.setGeometry(QtCore.QRect(10, 40, 41, 20))
        self.lineEdit_Iteration_No_Channel_2.setObjectName("lineEdit_Iteration_No_Channel_2")
        self.label_10 = QtWidgets.QLabel(self.groupBox_9)
        self.label_10.setGeometry(QtCore.QRect(10, 20, 71, 20))
        self.label_10.setObjectName("label_10")
        self.lineEdit_On_Time_Channel_2 = QtWidgets.QLineEdit(self.groupBox_9)
        self.lineEdit_On_Time_Channel_2.setGeometry(QtCore.QRect(100, 40, 41, 20))
        self.lineEdit_On_Time_Channel_2.setObjectName("lineEdit_On_Time_Channel_2")
        self.label_17 = QtWidgets.QLabel(self.groupBox_9)
        self.label_17.setGeometry(QtCore.QRect(90, 20, 41, 20))
        self.label_17.setObjectName("label_17")
        self.lineEdit_Off_Time_Channel_2 = QtWidgets.QLineEdit(self.groupBox_9)
        self.lineEdit_Off_Time_Channel_2.setGeometry(QtCore.QRect(150, 40, 41, 20))
        self.lineEdit_Off_Time_Channel_2.setObjectName("lineEdit_Off_Time_Channel_2")
        self.label_18 = QtWidgets.QLabel(self.groupBox_9)
        self.label_18.setGeometry(QtCore.QRect(150, 20, 41, 20))
        self.label_18.setObjectName("label_18")
        self.radioButton_Power_Cycle_Channel_2 = QtWidgets.QRadioButton(self.groupBox_9)
        self.radioButton_Power_Cycle_Channel_2.setGeometry(QtCore.QRect(200, 40, 71, 21))
        self.radioButton_Power_Cycle_Channel_2.setObjectName("radioButton_Power_Cycle_Channel_2")
        self.START_Channel_2 = QtWidgets.QPushButton(self.groupBox_2)
        self.START_Channel_2.setGeometry(QtCore.QRect(690, 40, 41, 31))
        self.START_Channel_2.setStyleSheet("")
        self.START_Channel_2.setObjectName("START_Channel_2")
        self.ineEdit_Current_Channel_2 = QtWidgets.QLineEdit(self.groupBox_2)
        self.ineEdit_Current_Channel_2.setGeometry(QtCore.QRect(50, 70, 41, 20))
        self.ineEdit_Current_Channel_2.setObjectName("ineEdit_Current_Channel_2")
        self.label_12 = QtWidgets.QLabel(self.groupBox_2)
        self.label_12.setGeometry(QtCore.QRect(10, 70, 41, 21))
        self.label_12.setObjectName("label_12")
        self.groupBox_3 = QtWidgets.QGroupBox(self.centralWidget)
        self.groupBox_3.setGeometry(QtCore.QRect(10, 240, 751, 111))
        self.groupBox_3.setObjectName("groupBox_3")
        self.ineEdit_Voltage_Channel_3 = QtWidgets.QLineEdit(self.groupBox_3)
        self.ineEdit_Voltage_Channel_3.setGeometry(QtCore.QRect(50, 30, 41, 20))
        self.ineEdit_Voltage_Channel_3.setObjectName("ineEdit_Voltage_Channel_3")
        self.label_11 = QtWidgets.QLabel(self.groupBox_3)
        self.label_11.setGeometry(QtCore.QRect(10, 30, 41, 21))
        self.label_11.setObjectName("label_11")
        self.groupBox_10 = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox_10.setGeometry(QtCore.QRect(100, 20, 301, 71))
        self.groupBox_10.setObjectName("groupBox_10")
        self.lineEdit_Time_Contineous_Channel_3 = QtWidgets.QLineEdit(self.groupBox_10)
        self.lineEdit_Time_Contineous_Channel_3.setGeometry(QtCore.QRect(10, 40, 41, 20))
        self.lineEdit_Time_Contineous_Channel_3.setObjectName("lineEdit_Time_Contineous_Channel_3")
        self.label_13 = QtWidgets.QLabel(self.groupBox_10)
        self.label_13.setGeometry(QtCore.QRect(10, 20, 81, 16))
        self.label_13.setObjectName("label_13")
        self.PAUSE_Channel_3 = QtWidgets.QPushButton(self.groupBox_10)
        self.PAUSE_Channel_3.setGeometry(QtCore.QRect(110, 30, 31, 31))
        self.PAUSE_Channel_3.setStyleSheet("")
        self.PAUSE_Channel_3.setObjectName("PAUSE_Channel_3")
        self.RESUME_Channel_3 = QtWidgets.QPushButton(self.groupBox_10)
        self.RESUME_Channel_3.setGeometry(QtCore.QRect(190, 30, 31, 31))
        self.RESUME_Channel_3.setStyleSheet("")
        self.RESUME_Channel_3.setObjectName("RESUME_Channel_3")
        self.radioButton_Contineous_Channel_3 = QtWidgets.QRadioButton(self.groupBox_10)
        self.radioButton_Contineous_Channel_3.setGeometry(QtCore.QRect(230, 40, 71, 20))
        self.radioButton_Contineous_Channel_3.setObjectName("radioButton_Contineous_Channel_3")
        self.groupBox_11 = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox_11.setGeometry(QtCore.QRect(410, 20, 271, 71))
        self.groupBox_11.setObjectName("groupBox_11")
        self.lineEdit_Iteration_No_Channel_3 = QtWidgets.QLineEdit(self.groupBox_11)
        self.lineEdit_Iteration_No_Channel_3.setGeometry(QtCore.QRect(10, 40, 41, 20))
        self.lineEdit_Iteration_No_Channel_3.setObjectName("lineEdit_Iteration_No_Channel_3")
        self.label_14 = QtWidgets.QLabel(self.groupBox_11)
        self.label_14.setGeometry(QtCore.QRect(10, 20, 71, 20))
        self.label_14.setObjectName("label_14")
        self.lineEdit_On_Time_Channel_3 = QtWidgets.QLineEdit(self.groupBox_11)
        self.lineEdit_On_Time_Channel_3.setGeometry(QtCore.QRect(90, 40, 41, 20))
        self.lineEdit_On_Time_Channel_3.setObjectName("lineEdit_On_Time_Channel_3")
        self.lineEdit_Off_Time_Channel_3 = QtWidgets.QLineEdit(self.groupBox_11)
        self.lineEdit_Off_Time_Channel_3.setGeometry(QtCore.QRect(150, 40, 41, 20))
        self.lineEdit_Off_Time_Channel_3.setObjectName("lineEdit_Off_Time_Channel_3")
        self.label_19 = QtWidgets.QLabel(self.groupBox_11)
        self.label_19.setGeometry(QtCore.QRect(90, 20, 41, 20))
        self.label_19.setObjectName("label_19")
        self.label_20 = QtWidgets.QLabel(self.groupBox_11)
        self.label_20.setGeometry(QtCore.QRect(150, 20, 41, 20))
        self.label_20.setObjectName("label_20")
        self.radioButton_Power_Cycle_Channel_3 = QtWidgets.QRadioButton(self.groupBox_11)
        self.radioButton_Power_Cycle_Channel_3.setGeometry(QtCore.QRect(200, 40, 71, 21))
        self.radioButton_Power_Cycle_Channel_3.setObjectName("radioButton_Power_Cycle_Channel_3")
        self.START_Channel_3 = QtWidgets.QPushButton(self.groupBox_3)
        self.START_Channel_3.setGeometry(QtCore.QRect(690, 40, 41, 31))
        self.START_Channel_3.setStyleSheet("")
        self.START_Channel_3.setObjectName("START_Channel_3")
        self.ineEdit_Current_Channel_3 = QtWidgets.QLineEdit(self.groupBox_3)
        self.ineEdit_Current_Channel_3.setGeometry(QtCore.QRect(50, 70, 41, 20))
        self.ineEdit_Current_Channel_3.setObjectName("ineEdit_Current_Channel_3")
        self.label_21 = QtWidgets.QLabel(self.groupBox_3)
        self.label_21.setGeometry(QtCore.QRect(10, 70, 41, 21))
        self.label_21.setObjectName("label_21")
        self.Log_File_Name = QtWidgets.QLineEdit(self.centralWidget)
        self.Log_File_Name.setGeometry(QtCore.QRect(60, 370, 171, 20))
        self.Log_File_Name.setObjectName("Log_File_Name")
        self.label_2 = QtWidgets.QLabel(self.centralWidget)
        self.label_2.setGeometry(QtCore.QRect(10, 370, 41, 16))
        self.label_2.setObjectName("label_2")
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 1115, 21))
        self.menuBar.setObjectName("menuBar")
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QtWidgets.QToolBar(MainWindow)
        self.mainToolBar.setObjectName("mainToolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.groupBox_7.setStyleSheet("QGroupBox#ColoredGroupBox { border: 1px solid red;}")
       

        '''
        PAUSE_RESUME_RUN-Button_Icon
        ----------------------------
        
        '''
##        self.PAUSE_Channel_1.setIcon(QIcon(QPixmap('C:/Users/3181/Desktop/final_power_supply/Power_Supply_IT_6322/BUTTONS/Pause_Button.jpg')))
##        self.START_Channel_1.setIcon(QIcon(QPixmap('C:/Users/3181/Desktop/final_power_supply/Power_Supply_IT_6322/BUTTONS/Play_Button.jpg')))
##        self.RESUME_Channel_1.setIcon(QIcon(QPixmap('C:/Users/3181/Desktop/final_power_supply/Power_Supply_IT_6322/BUTTONS/Resume_Buttons.jpg')))
##
##        self.PAUSE_Channel_2.setIcon(QIcon(QPixmap('C:/Users/3181/Desktop/final_power_supply/Power_Supply_IT_6322/BUTTONS/Pause_Button.jpg')))
##        self.START_Channel_2.setIcon(QIcon(QPixmap('C:/Users/3181/Desktop/final_power_supply/Power_Supply_IT_6322/BUTTONS/Play_Button.jpg')))
##        self.RESUME_Channel_2.setIcon(QIcon(QPixmap('C:/Users/3181/Desktop/final_power_supply/Power_Supply_IT_6322/BUTTONS/Resume_Buttons.jpg')))
##
##        self.PAUSE_Channel_3.setIcon(QIcon(QPixmap('C:/Users/3181/Desktop/final_power_supply/Power_Supply_IT_6322/BUTTONS/Pause_Button.jpg')))
##        self.START_Channel_3.setIcon(QIcon(QPixmap('C:/Users/3181/Desktop/final_power_supply/Power_Supply_IT_6322/BUTTONS/Play_Button.jpg')))
##        self.RESUME_Channel_3.setIcon(QIcon(QPixmap('C:/Users/3181/Desktop/final_power_supply/Power_Supply_IT_6322/BUTTONS/Resume_Buttons.jpg')))
    
        self.START_Channel_1.setStyleSheet('color: black')
        self.PAUSE_Channel_1.setStyleSheet('color: black')
        self.RESUME_Channel_1.setStyleSheet('color: black')
        self.START_Channel_2.setStyleSheet('color: black')
        self.PAUSE_Channel_2.setStyleSheet('color: black')
        self.RESUME_Channel_2.setStyleSheet('color: black')
        self.START_Channel_3.setStyleSheet('color: black')
        self.PAUSE_Channel_3.setStyleSheet('color: black')
        self.RESUME_Channel_3.setStyleSheet('color: black')
        '''
        RADIO-BUTTON-OPERATION
        ----------------------
        --->Switch between Contineous-Mode-OPERATION and Power-cycle-mode
            1>If Contineous-Mode is selected then Power-cycle-mode is Disable else Enable.
            2>For Channel-1,Channel-2 and Channel-3
        '''
        #For Channel-1
        self.radioButton_Contineous_Channel_1.clicked.connect(self.Enable_Disable)
        self.radioButton_Power_Cycle_Channel_1.clicked.connect(self.Enable_Disable)
        #For Channel-2
        self.radioButton_Contineous_Channel_2.clicked.connect(self.Enable_Disable)
        self.radioButton_Power_Cycle_Channel_2.clicked.connect(self.Enable_Disable)
        #For Channel-3
        self.radioButton_Contineous_Channel_3.clicked.connect(self.Enable_Disable)
        self.radioButton_Power_Cycle_Channel_3.clicked.connect(self.Enable_Disable)
        

        '''
        COMPORT-CONFIGURATION
        ---------------------
        Group Box Button
        ----------------
            --->In the group box button we have to show available com port name in order choose among one
            --->We will list the available com port using import serial packege contains tools.list_ports it will
            provide all the comport information
            --->In order to add the com port information in Group box we will use .addItem PYQT5 function
        '''
        connected = []
        Data_Item = []
        Com_Port_List =[]
        Com_Port_List = serial.tools.list_ports.comports()
        for Com_Id in Com_Port_List:
            #connected.append(Com_Id.device)
            self.Com_Port_Box.addItem(Com_Id.device)

        '''
        OPEN_COM_PORT Button
        --------------------
        --->Inorder to open the serial com port we will invoke signal slot using .clicked.connect(slot name) method.
        '''
        Baudrate = self.lineEdit_Baudrate.text()
        Current_Com_Port =str(self.Com_Port_Box.currentText())
        self.OPEN_COM_PORT.clicked.connect(lambda:self.Baudrate_Configuration())

        '''
        CHANNEL-1
        **********
        PAUSE
        -----
        RESUME
        -----
        STOP
        ----
        '''
        
        self.PAUSE_Channel_1.clicked.connect(lambda:self.PAUSE_RESUME_CHANNEL_1(self.PAUSE_Channel_1))
        self.RESUME_Channel_1.clicked.connect(lambda:self.PAUSE_RESUME_CHANNEL_1(self.RESUME_Channel_1))
        self.START_Channel_1.clicked.connect(lambda:self.PAUSE_RESUME_CHANNEL_1(self.START_Channel_1))

        '''
        CHANNEL-2
        **********
        PAUSE
        -----
        RESUME
        -----
        STOP
        ----
        '''
        
        self.PAUSE_Channel_2.clicked.connect(lambda:self.PAUSE_RESUME_CHANNEL_2(self.PAUSE_Channel_2))
        self.RESUME_Channel_2.clicked.connect(lambda:self.PAUSE_RESUME_CHANNEL_2(self.RESUME_Channel_2))
        self.START_Channel_2.clicked.connect(lambda:self.PAUSE_RESUME_CHANNEL_2(self.START_Channel_2))

        '''
        CHANNEL-3
        **********
        PAUSE
        -----
        RESUME
        -----
        STOP
        ----
        '''

      
        self.PAUSE_Channel_3.clicked.connect(lambda:self.PAUSE_RESUME_CHANNEL_3(self.PAUSE_Channel_3))
        self.RESUME_Channel_3.clicked.connect(lambda:self.PAUSE_RESUME_CHANNEL_3(self.RESUME_Channel_3))
        self.START_Channel_3.clicked.connect(lambda:self.PAUSE_RESUME_CHANNEL_3(self.START_Channel_3))


        '''
        SEND Button
        -----------
        --->Inorder to send message to com port(with specific message format) from channel-1
        --->channel-2
        --->channel-3
        ---:above three channels will work independetally
        ---:for that each channel will work on the thread base
        '''
##        Voltage = self.lineEdit_Voltage_Channel_1.text()#Input Voltage
##        #Curent = self.lineEdit_Curent_Channel_1.text() #Input current
##        Exicution_Time = self.lineEdit_Contineous_Channel_1.text() #Time_To_Exicute
##        Iteration_No = self.lineEdit_Iteration_No_Channel_1.text() #Iteration_No to be exicute
##        '''
##        ON & OFF TIME FOR POWER-CYCLE MODE ADD AFTER THE IMPLEMENTATION IS COMPLATE ON GUI SIDE
##        '''
##
        
        self.SEND.clicked.connect(lambda:self.SEND_MSG_CHANNEL_ONE())
        self.SEND.clicked.connect(lambda:self.SEND_MSG_CHANNEL_TWO())
        self.SEND.clicked.connect(lambda:self.SEND_MSG_CHANNEL_THREE())
        
##
##        '''
##        future implementation
##        ----------------------
##        self.SEND.clicked.connect(lambda:self.COM_PORT(Baudrate,Current_Com_Port))
##        self.SEND.clicked.connect(lambda:self.COM_PORT(Baudrate,Current_Com_Port))
##        '''
        
      
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def PAUSE_RESUME_CHANNEL_1(self,b):
        global END_TIME
        global START
        
        self.start =0
        self.END_TIME =0
        self.Loop_Time = 0
        
        self.stop_threads = False
        self.Click_Event =b
        vlc_info_thread = threading.Thread(target =self.Communication_Obj.SEND_MSG_CHANNEL_ONE_CON)
 
        #print("clicked button is>>>>>>>>>>>>"+ b.text())
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>self.SEND_BUTTON <<<<<<<<<<<<<<",self.SEND_BUTTON)
        if b.text() == "SEND":
            self.START =time.time()
            vlc_info_thread.start()

            
        #if b.isChecked:
        if b.text() == "WRITE":
            CHANNEL_ONE = threading.Thread(target = self.SEND_MSG_CHANNEL_ONE)
            self.START_Channel_1.setStyleSheet('color: green')
            self.PAUSE_Channel_1.setStyleSheet('color: black')
            self.RESUME_Channel_1.setStyleSheet('color: black')
            print("START BUTTON CLICKED")
            
            self.start =time.time()
            print("START BUTTON CLICKED",self.start)
            CHANNEL_ONE.start()
            
         

            
        #if b.isChecked:
        if b.text() == "PAU":
            self.PAUSE_Channel_1.setStyleSheet('color: green')
            self.RESUME_Channel_1.setStyleSheet('color: black')
            self.START_Channel_1.setStyleSheet('color: black')
            #print("PAUSE BUTTON CLICKED")
            self.Communication_Obj.UPDATE_FLAG_PAUSE()
            END_TIME = time.time()
            self.Communication_Obj.Update_Pause_Time(END_TIME)

            if not vlc_info_thread.isAlive(): 
                  print('thread killed')

                  
        #if b.isChecked:
        if b.text() == "RES":
            self.RESUME_Channel_1.setStyleSheet('color: green')
            self.PAUSE_Channel_1.setStyleSheet('color: black')
            self.START_Channel_1.setStyleSheet('color: black')

            
            
            self.Communication_Obj.Update_Time()

            self.start = time.time()
            self.start = datetime.datetime.fromtimestamp(self.start).strftime('%H:%M:%S.%f')
            self.Communication_Obj.UPDATE_FLAG_RESUME()
            #
            print("RESUME THREAD START")
            vlc_info_thread.start()
            

    
    def PAUSE_RESUME_CHANNEL_2(self,b):
        global END_TIME
        global START
        
        self.start =0
        self.END_TIME =0
        self.Loop_Time = 0
        
        self.stop_threads = False
        self.Click_Event =b
        
        Channel_Two = threading.Thread(target =self.Communication_Obj.SEND_MSG_CHANNEL_TWO_CON)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>self.SEND_BUTTON <<<<<<<<<<<<<<",self.SEND_BUTTON)
        if b.text() == "SEND":
            self.START =time.time()
            Channel_Two.start()

            
        #if b.isChecked:
        if b.text() == "WRITE":
            
            Channel_2 = threading.Thread(target =self.SEND_MSG_CHANNEL_TWO)
            self.START_Channel_1.setStyleSheet('color: green')
            self.PAUSE_Channel_1.setStyleSheet('color: black')
            self.RESUME_Channel_1.setStyleSheet('color: black')
            print("START BUTTON CLICKED")
            
            self.start =time.time()
            Channel_2.start()
            
            print("START BUTTON CLICKED",self.start)
           

        #if b.isChecked:
        if b.text() == "PAU":
            self.PAUSE_Channel_1.setStyleSheet('color: green')
            self.RESUME_Channel_1.setStyleSheet('color: black')
            self.START_Channel_1.setStyleSheet('color: black')
            #print("PAUSE BUTTON CLICKED")
            self.Communication_Obj.UPDATE_FLAG_PAUSE_CH2()
            END_TIME = time.time()
            self.Communication_Obj.Update_Pause_Time_CH2(END_TIME)
            if not Channel_Two.isAlive(): 
                  print('thread killed')

                  
        #if b.isChecked:
        if b.text() == "RES":
            self.RESUME_Channel_1.setStyleSheet('color: green')
            self.PAUSE_Channel_1.setStyleSheet('color: black')
            self.START_Channel_1.setStyleSheet('color: black')
            self.Communication_Obj.Update_Time_CH2()
            self.start = time.time()
            Channel_Two.start()
            self.start = datetime.datetime.fromtimestamp(self.start).strftime('%H:%M:%S.%f')
            self.Communication_Obj.UPDATE_FLAG_RESUME_CH2()
            #
            print("RESUME THREAD START")
            
            #Updating The Time for 

    
    def PAUSE_RESUME_CHANNEL_3(self,b):
        global END_TIME
        global START
        
        self.start =0
        self.END_TIME =0
        self.Loop_Time = 0
        
        self.stop_threads = False
        self.Click_Event =b
        
        Channel_3 = threading.Thread(target =self.Communication_Obj.SEND_MSG_CHANNEL_THREE_CON)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>self.SEND_BUTTON <<<<<<<<<<<<<<",self.SEND_BUTTON)
        if b.text() == "SEND":
            self.START =time.time()
            Channel_3.start()

            
        #if b.isChecked:
        if b.text() == "WRITE":
            Channel_Three = threading.Thread(target =self.SEND_MSG_CHANNEL_THREE)
            self.START_Channel_1.setStyleSheet('color: green')
            self.PAUSE_Channel_1.setStyleSheet('color: black')
            self.RESUME_Channel_1.setStyleSheet('color: black')
            print("START BUTTON CLICKED")
            self.start =time.time()
            print("START BUTTON CLICKED",self.start)
            Channel_Three.start()
            #vlc_info_thread.start()

        if b.text() == "PAU":
            self.PAUSE_Channel_1.setStyleSheet('color: green')
            self.RESUME_Channel_1.setStyleSheet('color: black')
            self.START_Channel_1.setStyleSheet('color: black')
            #print("PAUSE BUTTON CLICKED")
            self.Communication_Obj.UPDATE_FLAG_PAUSE_CH3()
            END_TIME = time.time()
            self.Communication_Obj.Update_Pause_Time_CH3(END_TIME)
            if not Channel_3.isAlive(): 
                  print('thread killed')

                  
        #if b.isChecked:
        if b.text() == "RES":
            self.RESUME_Channel_1.setStyleSheet('color: green')
            self.PAUSE_Channel_1.setStyleSheet('color: black')
            self.START_Channel_1.setStyleSheet('color: black')
            self.Communication_Obj.Update_Time_Ch3()
            self.start = time.time()
            self.start = datetime.datetime.fromtimestamp(self.start).strftime('%H:%M:%S.%f')
            self.Communication_Obj.UPDATE_FLAG_RESUME_CH3()
            #
            print("RESUME THREAD START")
            Channel_3.start()
            #Updating The Time for

            
            
    def Enable_Disable(self):
        #For Channel-1
        if self.radioButton_Contineous_Channel_1.isChecked() == True:
            self.radioButton_Contineous_Channel_1.setStyleSheet('color: red')
            self.groupBox_7.setStyleSheet('color: green')
            self.groupBox_6.setEnabled(False)
            
        else:
            self.groupBox_6.setEnabled(True)
            self.radioButton_Contineous_Channel_1.setStyleSheet('color: black')
            self.groupBox_7.setStyleSheet('color: black')

        if self.radioButton_Power_Cycle_Channel_1.isChecked() == True:
            self.radioButton_Power_Cycle_Channel_1.setStyleSheet('color: red')
            self.groupBox_6.setStyleSheet('color: green')
            self.groupBox_7.setEnabled(False)
        else:
            self.groupBox_7.setEnabled(True)
            self.radioButton_Power_Cycle_Channel_1.setStyleSheet('color: black')
            self.groupBox_6.setStyleSheet('color: black')

        #For Channel-2
        if self.radioButton_Contineous_Channel_2.isChecked() == True:
            self.radioButton_Contineous_Channel_2.setStyleSheet('color: red')
            self.groupBox_8.setStyleSheet('color: green')
            self.groupBox_9.setEnabled(False)
        else:
            self.groupBox_9.setEnabled(True)
            self.radioButton_Contineous_Channel_2.setStyleSheet('color: black')
            self.groupBox_8.setStyleSheet('color: black')

        if self.radioButton_Power_Cycle_Channel_2.isChecked() == True:
            self.radioButton_Power_Cycle_Channel_2.setStyleSheet('color: red')
            self.groupBox_9.setStyleSheet('color: green')
            self.groupBox_8.setEnabled(False)
        else:
            self.groupBox_8.setEnabled(True)
            self.radioButton_Power_Cycle_Channel_2.setStyleSheet('color: black')
            self.groupBox_9.setStyleSheet('color: black')

        #For Channel-2
        if self.radioButton_Contineous_Channel_3.isChecked() == True:
            self.radioButton_Contineous_Channel_3.setStyleSheet('color: red')
            self.groupBox_10.setStyleSheet('color: green')
            self.groupBox_11.setEnabled(False)
        else:
            self.groupBox_11.setEnabled(True)
            self.radioButton_Contineous_Channel_3.setStyleSheet('color: black')
            self.groupBox_10.setStyleSheet('color: black')

        if self.radioButton_Power_Cycle_Channel_3.isChecked() == True:
            self.groupBox_11.setStyleSheet('color: green')
            self.radioButton_Power_Cycle_Channel_3.setStyleSheet('color: red')
            self.groupBox_10.setEnabled(False)
        else:
            self.groupBox_10.setEnabled(True)
            self.radioButton_Power_Cycle_Channel_3.setStyleSheet('color: black')
            self.groupBox_11.setStyleSheet('color: black')
            
##    def UPDATED_LOG(self,Return_Log_Data):
##                self.Return_Log_Data = Return_Log_Data
##          
##                self.LOG_ENGINE(self.Return_Log_Data)
        
    def Baudrate_Configuration(self):
        self.OPEN_COM_PORT.setStyleSheet("background-color: green")
        Baudrate = self.lineEdit_Baudrate.text()
        Current_Com_Port =str(self.Com_Port_Box.currentText())
        print("Baudrate",Baudrate)
        print("Current_Com_Port",Current_Com_Port)
        
        #Calling the boudrate function
        Return_Value = self.Communication_Obj.COM_PORT(Baudrate,Current_Com_Port)
        

        self.Com_Open_label.setText(Return_Value)
        dt_string = now_1.strftime("%d/%m/%Y %H:%M:%S")
        Com_Msg = dt_string+">>>>>"+Current_Com_Port+"-"+Return_Value
        self.TextEdit_Tx_Data.append(Com_Msg)


    def SEND_MSG_CHANNEL_ONE(self):
                self.SEND_BUTTON = True
                self.SEND.setStyleSheet("background-color: green")
                self.START = 0
                self.Communication_Obj.UPDATE_FLAG_SEND()
                
                self.Voltage_Channel_1        = self.lineEdit_Voltage_Channel_1.text()#Input Voltage
                
                self.Exicution_Time_Channel_1 = self.lineEdit_Time_Contineous_Channel_1.text()
                print("self.Exicution_Time_Channel_1",self.Exicution_Time_Channel_1)
                '''
                In main _GUI Current editor has to be added
                -------------------------------------------
                '''
              
                '''
                Contineous-Mode-OPERATION
                -------------------------
                '''
                
                if not self.Voltage_Channel_1:
                    self.Voltage_Channel_1= 20
                else:
                    self.Voltage_Channel_1
                Voltage_Channel_1             = int(self.Voltage_Channel_1)
##                if int(Current_Channel_1) <0 :
##                    Current_Channel_1 = 1
##                else:
##                    Current_Channel_1


                    
                
                
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",self.radioButton_Power_Cycle_Channel_1.text())
                if self.radioButton_Contineous_Channel_1.isChecked() == True:
                    if self.radioButton_Contineous_Channel_1.text() == "Con-Mode":
                    
                                
                                #self.Curent                  = self.lineEdit_Curent_Channel_1.text() #Input current
                                self.Exicution_Time_Channel_1 = self.lineEdit_Time_Contineous_Channel_1.text() #Time_To_Exicute
                                
                                
                                if not self.Exicution_Time_Channel_1:
                                    self.Exicution_Time_Channel_1= 5
                                else:
                                    self.Exicution_Time_Channel_1
                                    
                                Exicution_Time_Channel_1      = int(self.Exicution_Time_Channel_1)
                                self.START = time.time()
                                self.Communication_Obj.SEND_MSG_CHANNEL_ONE_PARAMETERS(Voltage_Channel_1,Exicution_Time_Channel_1)
                                self.Communication_Obj.Updated_Start_Time(self.START)
                                Contineous_Channel_1 = threading.Thread(target =self.Communication_Obj.SEND_MSG_CHANNEL_ONE_CON)
                                Contineous_Channel_1.start()
                                #Return_Value = self.Communication_Obj.SEND_MSG_CHANNEL_ONE_CON(Voltage_Channel_1,Exicution_Time_Channel_1)
                                #self.TextEdit_Tx_Data.append(Return_Value)


                elif self.radioButton_Power_Cycle_Channel_1.isChecked() == True:           
                    if self.radioButton_Power_Cycle_Channel_1.text() == "Pow-Mode":
                                print("INSIDE THE radioButton_Power_Cycle_Channel_1")
                    
                        
                                self.Iteration_No_Channel_1 = self.lineEdit_Iteration_No_Channel_1.text() #Iteration_No to be exicute
                                if not self.Iteration_No_Channel_1:
                                    self.Iteration_No_Channel_1 = 5
                                else:
                                    self.Iteration_No_Channel_1
                                    
                                    
                                self.On_Time_Channel_1  = self.lineEdit_On_Time_Channel_1.text()
                                self.Off_Time_Channel_1 = self.lineEdit_Off_Time_Channel_1.text()

                                if not self.On_Time_Channel_1:
                                    self.On_Time_Channel_1 = 2
                                else:
                                    self.On_Time_Channel_1

                                if not self.Off_Time_Channel_1:
                                    self.Off_Time_Channel_1 = 2
                                else:
                                    self.Off_Time_Channel_1
                                    
                                Iteration_No_Channel_1 = int(self.Iteration_No_Channel_1)
                                On_Time_Channel_1 = int(self.On_Time_Channel_1)
                                Off_Time_Channel_1 = int(self.Off_Time_Channel_1)
                                print("self.Voltage_Channel_1",type(self.Voltage_Channel_1))


                                
                                self.Communication_Obj.SEND_MSG_CHANNEL_POW_PARAMETER(self.Iteration_No_Channel_1,self.On_Time_Channel_1,self.Off_Time_Channel_1,Voltage_Channel_1)
                                Power_Cycle_Channel_1 = threading.Thread(target =self.Communication_Obj.SEND_MSG_CHANNEL_POW_CYCLE)
                                Power_Cycle_Channel_1.start()

                                print("!>>>>>>>>>>>>>>>>>>>>>>>",Iteration_No_Channel_1,On_Time_Channel_1,Off_Time_Channel_1)
                                #Return_Value = self.Communication_Obj.SEND_MSG_CHANNEL_ONE_CON(Voltage_Channel_1,Exicution_Time_Channel_1)
                                #self.TextEdit_Tx_Data.append(Return_Value)
                else:
                    print("Channel ONE is not selected please select valid MODE :-1>Contineous Mode 2>Power_Cycle Mode")
                                
    def SEND_MSG_CHANNEL_TWO(self):
                self.SEND_BUTTON = True
                self.SEND.setStyleSheet("background-color: green")
                self.START = 0
                self.Communication_Obj.UPDATE_FLAG_SEND()
                
                self.Voltage_Channel_2        = self.ineEdit_Voltage_Channel_2.text()#Input Voltage
                
                self.Exicution_Time_Channel_2 = self.lineEdit_Time_Contineous_Channel_2.text()
                print("self.Exicution_Time_Channel_2",self.Exicution_Time_Channel_2)
                '''
                In main _GUI Current editor has to be added
                -------------------------------------------
                '''
              
                '''
                Contineous-Mode-OPERATION
                -------------------------
                '''
                
                if not self.Voltage_Channel_2:
                    self.Voltage_Channel_2= 20
                else:
                    self.Voltage_Channel_2
                Voltage_Channel_2             = int(self.Voltage_Channel_2)
##                if int(Current_Channel_1) <0 :
##                    Current_Channel_1 = 1
##                else:
##                    Current_Channel_1


                    
                
                
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",self.radioButton_Contineous_Channel_2.text())
                if self.radioButton_Contineous_Channel_2.isChecked() == True:
                    if self.radioButton_Contineous_Channel_2.text() == "Con-Mode":
                    
                                
                                #self.Curent                  = self.lineEdit_Curent_Channel_1.text() #Input current
                                self.Exicution_Time_Channel_2 = self.lineEdit_Time_Contineous_Channel_2.text() #Time_To_Exicute
                                
                                
                                if not self.Exicution_Time_Channel_2:
                                    self.Exicution_Time_Channel_2= 5
                                else:
                                    self.Exicution_Time_Channel_2
                                    
                                Exicution_Time_Channel_2      = int(self.Exicution_Time_Channel_2)

                                print("INSIDE THE radioButton_Contineous_Channel_2")
                                print("Exicution_Time_Channel_2",Exicution_Time_Channel_2)
                                print("Voltage_Channel_2",Voltage_Channel_2)
                             
                                

                                #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                self.START = time.time()
                                self.Communication_Obj.SEND_MSG_CHANNEL_TWO_PARAMETERS(Voltage_Channel_2,Exicution_Time_Channel_2)
                                self.Communication_Obj.Updated_Start_Time_CH2(self.START)
                                Contineous_Channel_2 = threading.Thread(target =self.Communication_Obj.SEND_MSG_CHANNEL_TWO_CON)
                                Contineous_Channel_2.start()
                                #Return_Value = self.Communication_Obj.SEND_MSG_CHANNEL_ONE_CON(Voltage_Channel_1,Exicution_Time_Channel_1)
                                #self.TextEdit_Tx_Data.append(Return_Value)
                                #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
##

                elif self.radioButton_Power_Cycle_Channel_2.isChecked() == True:           
                    if self.radioButton_Power_Cycle_Channel_2.text() == "Pow-Mode":
                                print("INSIDE THE radioButton_Power_Cycle_Channel_1")
                    
                        
                                self.Iteration_No_Channel_2 = self.lineEdit_Iteration_No_Channel_2.text() #Iteration_No to be exicute
                                if not self.Iteration_No_Channel_2:
                                    self.Iteration_No_Channel_2 = 5
                                else:
                                    self.Iteration_No_Channel_2
                                    
                                    
                                self.On_Time_Channel_2  = self.lineEdit_On_Time_Channel_2.text()
                                self.Off_Time_Channel_2 = self.lineEdit_Off_Time_Channel_2.text()

                                if not self.On_Time_Channel_2:
                                    self.On_Time_Channel_2 = 2
                                else:
                                    self.On_Time_Channel_2

                                if not self.Off_Time_Channel_2:
                                    self.Off_Time_Channel_2 = 2
                                else:
                                    self.Off_Time_Channel_2
                                    
                                Iteration_No_Channel_2 = int(self.Iteration_No_Channel_2)
                                On_Time_Channel_2 = int(self.On_Time_Channel_2)
                                Off_Time_Channel_2 = int(self.Off_Time_Channel_2)
                                print("self.Voltage_Channel_2",type(self.Voltage_Channel_2))
                                print("INSIDE THE radioButton_Power_Cycle_Channel_2")
                                print("Iteration_No_Channel_2",Iteration_No_Channel_2)
                                print("On_Time_Channel_2",On_Time_Channel_2)
                                print("On_Time_Channel_2",Off_Time_Channel_2)
                                

                        
                                #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                self.Communication_Obj.SEND_MSG_CHANNEL_POW_PARAMETER_CH2(self.Iteration_No_Channel_2,self.On_Time_Channel_2,self.Off_Time_Channel_2,Voltage_Channel_2)
                                Power_Cycle_Channel_2 = threading.Thread(target =self.Communication_Obj.SEND_MSG_CHANNEL_POW_CYCLE_CH2)
                                Power_Cycle_Channel_2.start()
                                #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
##
##                                print("!>>>>>>>>>>>>>>>>>>>>>>>",Iteration_No_Channel_2,On_Time_Channel_2,Off_Time_Channel_2)
##                                #Return_Value = self.Communication_Obj.SEND_MSG_CHANNEL_ONE_CON(Voltage_Channel_1,Exicution_Time_Channel_1)
##                                #self.TextEdit_Tx_Data.append(Return_Value)
                else:
                    print("Channel TWO is not selected please select valid MODE :-1>Contineous Mode 2>Power_Cycle Mode")

                
    def SEND_MSG_CHANNEL_THREE(self):
        
                self.SEND_BUTTON = True
                self.SEND.setStyleSheet("background-color: green")
                self.START = 0
                self.Communication_Obj.UPDATE_FLAG_SEND()
                
                self.Voltage_Channel_3        = self.ineEdit_Voltage_Channel_3.text()#Input Voltage
                
                self.Exicution_Time_Channel_3 = self.lineEdit_Time_Contineous_Channel_3.text()
                print("self.Exicution_Time_Channel_3",self.Exicution_Time_Channel_3)
                '''
                In main _GUI Current editor has to be added
                -------------------------------------------
                '''
              
                '''
                Contineous-Mode-OPERATION
                -------------------------
                '''
                
                if not self.Voltage_Channel_3:
                    self.Voltage_Channel_3= 20
                else:
                    self.Voltage_Channel_3
                Voltage_Channel_3             = int(self.Voltage_Channel_3)
##                if int(Current_Channel_1) <0 :
##                    Current_Channel_1 = 1
##                else:
##                    Current_Channel_1


                    
                
                
                
                if self.radioButton_Contineous_Channel_3.isChecked() == True:
                    if self.radioButton_Contineous_Channel_3.text() == "Con-Mode":
                    
                                
                                #self.Curent                  = self.lineEdit_Curent_Channel_1.text() #Input current
                                self.Exicution_Time_Channel_3 = self.lineEdit_Time_Contineous_Channel_3.text() #Time_To_Exicute
                                
                                
                                if not self.Exicution_Time_Channel_3:
                                    self.Exicution_Time_Channel_3= 5
                                else:
                                    self.Exicution_Time_Channel_3
                                    
                                Exicution_Time_Channel_3      = int(self.Exicution_Time_Channel_3)
                                self.START = time.time()
                                
                                print("!!!!radioButton_Contineous_Channel_3!!!!!",self.radioButton_Contineous_Channel_3.text())
                                print("Exicution_Time_Channel_3",Exicution_Time_Channel_3)
                                print("Exicution_Time_Channel_3",Voltage_Channel_3)
                                

                                

                                #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                self.Communication_Obj.SEND_MSG_CHANNEL_THREE_PARAMETERS(Voltage_Channel_3,Exicution_Time_Channel_3)
                                self.Communication_Obj.Updated_Start_Time(self.START)
                                Contineous_Channel_1 = threading.Thread(target =self.Communication_Obj.SEND_MSG_CHANNEL_THREE_CON)
                                Contineous_Channel_1.start()
                                
##                                #Return_Value = self.Communication_Obj.SEND_MSG_CHANNEL_ONE_CON(Voltage_Channel_1,Exicution_Time_Channel_1)
##                                #self.TextEdit_Tx_Data.append(Return_Value)
##                                #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
##                                
##
                


                
                elif self.radioButton_Power_Cycle_Channel_3.isChecked() == True:           
                    if self.radioButton_Power_Cycle_Channel_3.text() == "Pow-Mode":
                               
                    
                        
                                self.Iteration_No_Channel_3 = self.lineEdit_Iteration_No_Channel_3.text() #Iteration_No to be exicute
                                if not self.Iteration_No_Channel_3:
                                    self.Iteration_No_Channel_3 = 5
                                else:
                                    self.Iteration_No_Channel_3
                                    
                                    
                                self.On_Time_Channel_3  = self.lineEdit_On_Time_Channel_3.text()
                                self.Off_Time_Channel_3 = self.lineEdit_Off_Time_Channel_3.text()

                                if not self.On_Time_Channel_3:
                                    self.On_Time_Channel_3 = 2
                                else:
                                    self.On_Time_Channel_3

                                if not self.Off_Time_Channel_3:
                                    self.Off_Time_Channel_3 = 2
                                else:
                                    self.Off_Time_Channel_3
                                    
                                Iteration_No_Channel_3 = int(self.Iteration_No_Channel_3)
                                On_Time_Channel_3 = int(self.On_Time_Channel_3)
                                Off_Time_Channel_3 = int(self.Off_Time_Channel_3)

                                
                                print("self.Voltage_Channel_3",type(self.Voltage_Channel_3))
                                print("INSIDE THE radioButton_Power_Cycle_Channel_3")
                                print("Iteration_No_Channel_3",Iteration_No_Channel_3)
                                print("On_Time_Channel_3",On_Time_Channel_3)
                                print("On_Time_Channel_3",Off_Time_Channel_3)
                                


                                self.Communication_Obj.SEND_MSG_CHANNEL_POW_PARAMETER_THREE(self.Iteration_No_Channel_3,self.On_Time_Channel_3,self.Off_Time_Channel_3,Voltage_Channel_3)
                                Power_Cycle_Channel_3 = threading.Thread(target =self.Communication_Obj.SEND_MSG_CHANNEL_POW_CYCLE_CH3)
                                Power_Cycle_Channel_3.start()

                                print("!>>>>>>>>>>>>>>>>>>>>>>>",Iteration_No_Channel_3,On_Time_Channel_3,Off_Time_Channel_3)
##                                #Return_Value = self.Communication_Obj.SEND_MSG_CHANNEL_ONE_CON(Voltage_Channel_1,Exicution_Time_Channel_1)
##                                #self.TextEdit_Tx_Data.append(Return_Value)

                        
                else:
                    print("Channel THREE is not selected please select valid MODE :-1>Contineous Mode 2>Power_Cycle Mode")
                        

        
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.groupBox.setTitle(_translate("MainWindow", "CHANNEL-1"))
        self.label.setText(_translate("MainWindow", "Voltage"))
        self.groupBox_6.setTitle(_translate("MainWindow", "Power-Cycle-Mode"))
        self.radioButton_Power_Cycle_Channel_1.setText(_translate("MainWindow", "Pow-Mode"))
        self.label_16.setText(_translate("MainWindow", "Off-Time"))
        self.label_15.setText(_translate("MainWindow", "On-Time"))
        self.label_5.setText(_translate("MainWindow", "Iteration -No"))
        self.groupBox_7.setTitle(_translate("MainWindow", "Contineous-Mode"))
        self.label_4.setText(_translate("MainWindow", "TIME-(Minutes)"))
        self.radioButton_Contineous_Channel_1.setText(_translate("MainWindow", "Con-Mode"))
        self.groupBox_4.setTitle(_translate("MainWindow", "Serial_communication"))
        self.OPEN_COM_PORT.setText(_translate("MainWindow", "OPEN"))
        self.SEND.setText(_translate("MainWindow", "SEND"))
        self.CLOSE.setText(_translate("MainWindow", "CLOSE"))
        self.label_7.setText(_translate("MainWindow", "<html><head/><body><p>PORT</p></body></html>"))
        self.label_Baudrate.setText(_translate("MainWindow", "<html><head/><body><p>BAUDRATE</p></body></html>"))
        self.label_3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">TX_LOGS</span></p></body></html>"))
        self.groupBox_2.setTitle(_translate("MainWindow", "CHANNEL-2"))
        self.label_6.setText(_translate("MainWindow", "Voltage"))
        self.groupBox_8.setTitle(_translate("MainWindow", "Contineous-Mode"))
        self.label_9.setText(_translate("MainWindow", "TIME-(Minutes)"))
        self.radioButton_Contineous_Channel_2.setText(_translate("MainWindow", "Con-Mode"))
        self.groupBox_9.setTitle(_translate("MainWindow", "Power-Cycle-Mode"))
        self.label_10.setText(_translate("MainWindow", "Iteration -No"))
        self.label_17.setText(_translate("MainWindow", "On-Time"))
        self.label_18.setText(_translate("MainWindow", "Off-Time"))
        self.radioButton_Power_Cycle_Channel_2.setText(_translate("MainWindow", "Pow-Mode"))
        self.groupBox_3.setTitle(_translate("MainWindow", "CHANNEL3"))
        self.label_11.setText(_translate("MainWindow", "Voltage"))
        self.groupBox_10.setTitle(_translate("MainWindow", "Contineous-Mode"))
        self.label_13.setText(_translate("MainWindow", "TIME-(Minutes)"))
        self.radioButton_Contineous_Channel_3.setText(_translate("MainWindow", "Con-Mode"))
        self.groupBox_11.setTitle(_translate("MainWindow", "Power-Cycle-Mode"))
        self.label_14.setText(_translate("MainWindow", "Iteration -No"))
        self.label_19.setText(_translate("MainWindow", "On-Time"))
        self.label_20.setText(_translate("MainWindow", "Off-Time"))
        self.radioButton_Power_Cycle_Channel_3.setText(_translate("MainWindow", "Pow-Mode"))
        self.label_2.setText(_translate("MainWindow", "Log-File"))
 





    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.groupBox.setTitle(_translate("MainWindow", "CHANNEL-1"))
        self.label.setText(_translate("MainWindow", "Voltage"))
        self.groupBox_6.setTitle(_translate("MainWindow", "Power-Cycle-Mode"))
        self.radioButton_Power_Cycle_Channel_1.setText(_translate("MainWindow", "Pow-Mode"))
        self.label_16.setText(_translate("MainWindow", "Off-Time"))
        self.label_15.setText(_translate("MainWindow", "On-Time"))
        self.label_5.setText(_translate("MainWindow", "Iteration -No"))
        self.groupBox_7.setTitle(_translate("MainWindow", "Contineous-Mode"))
        self.label_4.setText(_translate("MainWindow", "TIME-(Minutes)"))
        self.RESUME_Channel_1.setText(_translate("MainWindow", "RES"))
        self.radioButton_Contineous_Channel_1.setText(_translate("MainWindow", "Con-Mode"))
        self.PAUSE_Channel_1.setText(_translate("MainWindow", "PAU"))
        self.START_Channel_1.setText(_translate("MainWindow", "WRITE"))
        self.label_8.setText(_translate("MainWindow", "Current"))
        self.groupBox_4.setTitle(_translate("MainWindow", "Serial_communication"))
        self.OPEN_COM_PORT.setText(_translate("MainWindow", "OPEN"))
        self.SEND.setText(_translate("MainWindow", "SEND"))
        self.CLOSE.setText(_translate("MainWindow", "CLOSE"))
        self.label_7.setText(_translate("MainWindow", "<html><head/><body><p>PORT</p></body></html>"))
        self.label_Baudrate.setText(_translate("MainWindow", "<html><head/><body><p>BAUDRATE</p></body></html>"))
        self.label_3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">TX_LOGS</span></p></body></html>"))
        self.groupBox_2.setTitle(_translate("MainWindow", "CHANNEL-2"))
        self.label_6.setText(_translate("MainWindow", "Voltage"))
        self.groupBox_8.setTitle(_translate("MainWindow", "Contineous-Mode"))
        self.label_9.setText(_translate("MainWindow", "TIME-(Minutes)"))
        self.PAUSE_Channel_2.setText(_translate("MainWindow", "PAU"))
        self.RESUME_Channel_2.setText(_translate("MainWindow", "RES"))
        self.radioButton_Contineous_Channel_2.setText(_translate("MainWindow", "Con-Mode"))
        self.groupBox_9.setTitle(_translate("MainWindow", "Power-Cycle-Mode"))
        self.label_10.setText(_translate("MainWindow", "Iteration -No"))
        self.label_17.setText(_translate("MainWindow", "On-Time"))
        self.label_18.setText(_translate("MainWindow", "Off-Time"))
        self.radioButton_Power_Cycle_Channel_2.setText(_translate("MainWindow", "Pow-Mode"))
        self.START_Channel_2.setText(_translate("MainWindow", "WRITE"))
        self.label_12.setText(_translate("MainWindow", "Current"))
        self.groupBox_3.setTitle(_translate("MainWindow", "CHANNEL3"))
        self.label_11.setText(_translate("MainWindow", "Voltage"))
        self.groupBox_10.setTitle(_translate("MainWindow", "Contineous-Mode"))
        self.label_13.setText(_translate("MainWindow", "TIME-(Minutes)"))
        self.PAUSE_Channel_3.setText(_translate("MainWindow", "PAU"))
        self.RESUME_Channel_3.setText(_translate("MainWindow", "RES"))
        self.radioButton_Contineous_Channel_3.setText(_translate("MainWindow", "Con-Mode"))
        self.groupBox_11.setTitle(_translate("MainWindow", "Power-Cycle-Mode"))
        self.label_14.setText(_translate("MainWindow", "Iteration -No"))
        self.label_19.setText(_translate("MainWindow", "On-Time"))
        self.label_20.setText(_translate("MainWindow", "Off-Time"))
        self.radioButton_Power_Cycle_Channel_3.setText(_translate("MainWindow", "Pow-Mode"))
        self.START_Channel_3.setText(_translate("MainWindow", "WRITE"))
        self.label_21.setText(_translate("MainWindow", "Current"))
        self.label_2.setText(_translate("MainWindow", "Log-File"))


if __name__ == '__main__':
                        app = QtWidgets.QApplication(sys.argv)
                        MainWindow = QtWidgets.QMainWindow()
                        splash = QSplashScreen(QPixmap('Tessolve_normal.jpg'))
                        splash.show()
                        window1 = QtWidgets.QWidget()
                        time.sleep(0.2)
                        splash.finish(window1)
                        window = Ui_MainWindow()
                        window.setupUi(MainWindow)
                        MainWindow.show()
                        sys.exit(app.exec_())


